var modelPath   = global.srvConfig.modelDir;

var db = require(modelPath + 'db.js')('cms');
var UT  = require(modelPath + 'util.js');
var log = UT.log;

var STR = JSON.stringify;

//var dbaa = require('../tools/models/dbaa.js');
var upd = require(modelPath + 'updates.js');

var stockCat = {};
var stockDef = {};
var nextCatMask   = 0;
var nextMaskCatItem = {};

function loadCats(cb) {
    stockDef = {};
    nextCatMask = 0;
    nextMaskCatItem = {};
    var catSql = "SELECT * FROM stock_def_cat ORDER BY mask";

    db.query(catSql,[])
    .then(function(catResp) {
        var tmpMask = 1;

        catResp.map( function(cr) {
            nextCatMask > 0 || cr.mask == tmpMask || (nextCatMask = tmpMask);
            tmpMask = tmpMask * 2;
        });
        nextCatMask == 0 && (nextCatMask = tmpMask);

        var sql = "SELECT d.name AS name, dc.name AS cat_name, d.mask AS mask, dc.mask AS cat_mask " +
                  "FROM stock_def d RIGHT JOIN stock_def_cat dc ON (d.cat_mask=dc.mask) " + 
                  "ORDER BY d.cat_mask, d.mask";

        UT.db.queryAndCb( sql, [], function(resp) {
            resp = resp.data;
            var masks = {};
            resp.map(function(s) {
                typeof stockDef[s.cat_mask] == 'undefined' && (stockDef[s.cat_mask] = {name: s.cat_name, items: {}});
                stockDef[s.cat_mask].items[s.mask] = s.name; 
                stockDef[s.cat_mask].name = s.cat_name; 

                typeof masks[s.cat_mask] == 'undefined' && (masks[s.cat_mask] = []);
                masks[s.cat_mask].push(s.mask);
            });

            for (var i in masks) {
                nextMaskCatItem[i] = 0;
                tmpMask = 1;
                masks[i][0] === null || masks[i].map( m => {
                    nextMaskCatItem[i] > 0 || m == tmpMask || (nextMaskCatItem[i]= tmpMask);
                    tmpMask = tmpMask * 2;
                });
                nextMaskCatItem[i] == 0 && (nextMaskCatItem[i] = tmpMask);
            }

            typeof cb == 'function' && cb(stockDef);
        });
    })
    .catch(e => UT.log.err( e  ));
}
loadCats();

function checkFull(i, checks) {
    var cks = 0;
    for (var j in checks) 
        checks[j] && (cks++);

    //return cks > 0 && cks != Object.keys(stockDef[i].items).length;
    return true;
}

function resp(data, success) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}

var mod = module.exports = {
        getDefs: function(q, cb) {
            cb(resp(stockDef, true));
        },
        saveCat: function(q, cb) {
            if (q.cat_id=='9999' || typeof q.cat_id == 'undefined') { 
                var sql = "INSERT INTO stock_def_cat (name, mask) VALUES ($1, $2) RETURNING uid, mask";
                var data = [q.name, nextCatMask];
            } else {
                var sql = "UPDATE stock_def_cat SET name=$1 WHERE mask=$2";
                var data = [q.name, q.cat_id];
            }

            UT.db.queryAndCb( sql, data, function( catResp ) {
                if (q.cat_id == '9999')
                    mod.saveCatItem({item_id:'9999', name: 'new item', cat_mask: catResp.data[0].mask}, cb);
                else 
                    setTimeout( function() {
                        loadCats(function(resp) { cb(resp); });
                    }, 200);
            });
        },
        saveCatItem: function(q, cb) {
            UT.log.r( q, ' q in  save item' );
            if (q.item_id!='9999') { 
                var sql = "UPDATE stock_def SET name=$1 WHERE mask=$2 AND cat_mask=$3 RETURNING *";
                var data = [q.name, q.item_id, q.cat_mask];
            } else {
                var sql = "INSERT INTO stock_def (name, mask, cat_mask) VALUES ($1, $2, $3) RETURNING *";
                var nextMask = nextMaskCatItem[q.cat_mask];
                UT.log.lg('neext item mask', typeof nextMask);
                typeof nextMask == 'undefined' && (nextMask = 1);
                var data = [q.name, nextMask, q.cat_mask];
            }
            
            UT.db.queryAndCb( sql, data, () => loadCats(cb));
        },
        delCat: function(q, cb) {
            var sql = "DELETE FROM stock_def_cat WHERE mask=$1";
            var data = [q.mask];
            UT.db.queryAndCb( sql, data, cb);

            UT.log.lg( data , sql );
            UT.db.queryAndCb( sql, data, () => loadCats(cb) );
        },
        delCatItem: function(q, cb) {
            UT.log.lg( q , 'q in delCatItem');
            var sql = "DELETE FROM stock_def WHERE mask=$1 AND cat_mask=$2";
            var data = [q.mask, q.cat_mask];

            UT.log.lg( data , sql );
            UT.db.queryAndCb( sql, data, function() {
                //loadCats(cb)
                setTimeout( function() {
                    loadCats(function(resp) { cb(resp); });
                }, 200);
            });
        },

        getStock: function(q, cb) {
            //"SELECT * FROM stock WHERE tags->'2' ?| '{"8", "4"}'"

            var whereClause = '';
            for (var i in q.checks) {
                var itemClause = '';
                if (checkFull(i, q.checks[i]))
                    for (var j in q.checks[i]) {
                        if (q.checks[i][j]) {
                            itemClause += ', "' + j + '"';
                        }
                    }
                if (itemClause) 
                    whereClause += " AND tags->'" + i + "' ?| '{" + itemClause.substring(2) + "}'";
            }

            if (whereClause.length) {
                whereClause = whereClause.substring(4);
                whereClause = ' WHERE ' + whereClause;
            }

            var contSearch = '';
            if (typeof q.contentSearch != 'undefined' && q.contentSearch.length > 1) {
                contSearch = " (description ILIKE '%" + q.contentSearch + "%' " +
                             " OR name ILIKE '%" + q.contentSearch + "%') ";

                if (whereClause.length) 
                    whereClause += 'AND' + contSearch;
                else 
                    whereClause = 'WHERE' + contSearch;
            }

            var sql ="SELECT * FROM stock " + whereClause + " ORDER BY created DESC";
            UT.log.lg( sql );

            UT.db.queryAndCb( sql, [], cb);
        },
        genStock: function(q) {
            var rootPath = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front';
            mod.getStock(q, function(r ) {
                UT.log.lg( r , ' r ');
                require('fs').writeFile(rootPath + '/js/gen/stock.json', JSON.stringify(r.data, null,4), function(e, r){
                    console.log( arguments , rootPath + '/js/gen/stock.json');
                });
            });
            require('child_process').exec('cd ' + rootPath + '; bin/ver');
        },
        saveStock: function(q, cb) {
            UT.log.dbg( q );
            var upd1 = upd.gen(q, 'stock');
            UT.log.lg( upd1 );
            UT.db.queryAndCb( upd1.sql, upd1.data, cb);
            mod.genStock(q);
        },
        delItem : function(q, cb) {
            var sql = "DELETE FROM stock WHERE uid=$1 RETURNING uid";
            UT.db.queryAndCb( sql, [q.uid], cb);
        },
        ppHook: function(q, cb) {
            UT.log.lg( 'Paypal hook resp', q );
            cb(resp(true, true));
        }
}
