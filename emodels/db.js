var GLOB = global.srvConfig;
var myDb = null;
var pg=require('pg');
new pg.Client(GLOB.url).connect(function(er, con) { myDb = con; });

//var log=require('./log.js')();
log = { lg:function(arg) { console.log( arg ); } }

var fs=require('fs');
var types = pg.types;
types.setTypeParser(1114, function(stringValue) {
    return stringValue;
});

var dbs = null;

function loadQueries() {
    var queries = fs.readFileSync(__dirname + '/queries/queries.sql', 'utf8').split('>>>');;
    queries.shift();
    var queriesObj = {};
    queries.map(function(q) {
        var qq = q.split('<<<');
        queriesObj[qq[0].trim()] = qq[1];
    });
    return queriesObj;
}

function getConn(user, passwd, dbName, cb) {
    passwd == null || (user += ":" + passwd);
    var connStr = "postgres://" + user + '@localhost/' + dbName;
    new pg.Client(connStr).connect(function(err, conn) {
        err == null || console.log( 'ERR' , err );
        typeof cb == 'undefined' || cb(err==null ? conn : null);
    })
};

function getSuConn(cb) {
    getConn('dbcreator', '6fb2ef0b81759228bb681e29468e8920', 'rideadmin', cb);
};

function qFormat(q, vals, isUpdate) {
    isUpdate == typeof isUpdate != 'undefined' && isUpdate;
    for (var i=0; i<vals.length; i++)
        q = q.replace(new RegExp('\\{' +  i + '\\}', 'g'), vals[i]);

    if (isUpdate) {
        q = q.replace(new RegExp("'null'", 'g'), 'null');
    } else {
        //q = q.replace('null', ' IS NULL ');
    }
    return q;
};

function qFmt(set, isUpdate) {
    isUpdate = typeof isUpdate != 'undefined' && isUpdate;

    var cols = '';
    var vals = '';

    var upd = ''
    for (var i in set) {
        var val = set[i][0];
        if (set[i][1] == 'ok empty' && typeof val == 'undefined' || typeof val == null) val = '';
        if (val || set[i][1] != 'not empty') {
            typeof set[i][1] == 'number' && !set[i][0] && (val = set[i][1]);
            var quote = typeof set[i][1] == 'undefined' || set[i][1] ? "'" : '';
            set[i][1] == 'no quote' && (quote = '');
            cols += ', ' + i; 
            vals += ', ' + quote + val + quote;
            upd  += ', ' + i + '=' + quote +  val + quote;
        }
    }
    return isUpdate ? upd.substring(2)
                    : '(' + cols.substring(2) + ') VALUES (' + vals.substring(2) + ')';
};

function multiInsert(cols, rows) {
    var out = "";
    var rval = "";
    var vals = "";
    var oneVal;
    for (var i=0; i<cols.length; i++) out  += ","  + cols[i].trim();

    for (var j=0; j<rows.length; j++) {
        rval = '';
        for (var i=0; i<cols.length; i++) {
            oneVal = rows[j][cols[i]];
            typeof oneVal == 'string' && (oneVal = oneVal.trim());
            rval += "','" + oneVal;
        }
        vals += "),(" + rval.substr(2) + "'";
    }
    out = '(' + out.substr(1) + ") VALUES (" + vals.substr(3) + ');';
    return out;
};

var nedb = null;

module.exports = function(db) {
    return {
        conn:           function(db) { return dbs[db]; },
        getQuery:       function(handle) { return allQueries[handle] },
        query:          function(sql, valsAr, cb, erCb) { 
                            if (typeof sql != 'string') {
                                sql    = sql[0];
                                valsAr = sql[1];
                            }
                            return new Promise( (y,n) => {
                                myDb.query(sql, valsAr, (e,r) => {
                                    e && typeof erCb == 'function' && erCb(e);
                                    e || typeof cb   == 'function' && cb(r.rows);
                                    e ? n(e) : y(r.rows);
                                }) 
                            });
                        },
        q:              function(sql, cb, erCb) { 
                            myDb.query(sql, function(e,r) {
                                e && typeof erCb == 'function' && erCb(e);
                                e || typeof cb   == 'function' && cb(r.rows);
                            }) 
                        },
        qDb:            function(db, sql, cb, erCb) { 
                            return dbs[db].query(sql, function(e,r) {
                                e && require('fs').appendFile('./log/sqlErr.log', JSON.stringify(e));
                                e && typeof erCb == 'function' && erCb(e);
                                e || typeof cb   == 'function' && cb(r.rows);
                            }) 
                        },
        multiInsert:    multiInsert,
        qFormat:        qFormat,
        qFmt:           qFmt,
        getConn:        getConn,
        getSuConn:      getSuConn,
        dbUuid:         'md5(random()::text || clock_timestamp()::text)::uuid',
        getNe:          function(name) { 
            typeof name == 'undefined' && (name = 'auth');
            if (nedb == null) {
                var DS = require('nedb');
                nedb = new DS({filename: '/ram/ne/' + GLOB.inst + '.db', autoload: true});
                nedb.persistence.setAutocompactionInterval(10000);
            }
            return nedb;
        },
        neConfig: {
            get: function(cb) {
                var NE =  require('nedb');
                var neconfig = new NE({filename: '/ram/ne/ruleconfig.db', autoload: true}); 
                neconfig.find({}, function(e, r) { cb(r); });
            },
            put: function(obj, cb) {
                var NE =  require('nedb');
                var neconfig = new NE({filename: '/ram/ne/ruleconfig.db', autoload: true}); 
                neconfig.remove({}, function(re, rr) {
                    neconfig.insert(obj, function(ie, ir) { cb(ie ? ie : ir); });
                });
            }
        }
    }
}
