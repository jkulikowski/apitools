var modelPath   = global.srvConfig.modelDir;

var db = require(modelPath + 'db.js')('cms');
var UT  = require(modelPath + 'util.js');
var log = UT.log;

//var dbaa = require('../tools/models/dbaa.js');
var upd = require(modelPath + 'updates.js');

function resp(data, success) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}

var allBlogs = null;
function loadBlogIds() {
    allBlogs === null &&  UT.db.queryAndCb("SELECT uid, handle FROM blog",[], function(ret) {
        allBlogs = {};
        ret.data.map( function(b) {
            allBlogs[b.handle] = b.uid;
        });
        //UT.log.lg( 'ADD BLOGS', allBlogs);
        require('fs').writeFile('./prerender/blogs.json', JSON.stringify(allBlogs, null, 4), function(e,r) {
            e && console.log( 'prerender error', e );
            e || console.log( 'prerender WORKED' );
        });
        typeof cb == 'function' && cb();
    });
}

setTimeout( function() { 
    loadBlogIds(function() { 
        //UT.log.br( 'Blog Ids', allBlogs ); 
    });
}, 600);

var mod = module.exports = {
    get:   function(q, cb) { 
        var sql =   "SELECT b.uid, b.cat_uid, cat_name, sub_cat_name, sub_sub_cat_name, cont " +
                    "FROM blog b join blog_cat c ON (b.cat_uid=c.uid) " + 
                    "WHERE owner_uid =$1";
        db.query(sql,[q.owner_uid])
        .then(blogResp => {
            var ret = {};
            blogResp.map( function(b) {
                typeof ret[b.cat_name] == 'undefined' && (ret[b.cat_name] = {});
                typeof ret[b.cat_name][b.sub_cat_name] == 'undefined' && (ret[b.cat_name][b.sub_cat_name] = {});
                typeof ret[b.cat_name][b.sub_cat_name][b.sub_sub_cat_name]  == 'undefined' && (ret[b.cat_name][b.sub_cat_name][b.sub_sub_cat_name] = {uid: b.cat_uid, items:[]});
                ret[b.cat_name][b.sub_cat_name][b.sub_sub_cat_name].items.push(b);
            });
            
            cb(resp(ret, true));
        })
        .catch( e => cb( resp(e, false)));
    },
    genBlogs: function(q) {
        var rootPath = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front';
        me.getBlogs(q, function(r ) {
            require('fs').writeFile(rootPath + '/js/gen/blog.json', JSON.stringify(r.data, null,4), () => null);
        });
        require('child_process').exec('cd ' + rootPath + '; bin/ver');
    },
    saveBlog: function(q, cb) {
        q.handle = q.title.replace(/ /g,'-').replace(/'/g, '');

        var params = upd.gen(q, 'blog');
        UT.db.queryAndCb( params.sql, params.data, cb);
        setTimeout(loadBlogIds, 400);
    },
    reorderBlogs: function(q, cb) {
        var sql = q.uids.map( function(uid, i) {
            return "UPDATE blog SET seq=" + (i+1) + " WHERE uid='" + uid + "'";
        }).join(';');
        UT.db.queryAndCb(sql, [], cb);
    },
    delBlog: function(q, cb) {
        var b_item_sql = "DELETE FROM blog_item WHERE blog_uid=$1";
        var b_sql = "DELETE FROM blog WHERE uid=$1";
        var data = [q.uid];
        UT.db.queryAndCb( b_item_sql, data, () =>
            UT.db.queryAndCb( b_sql, data, () => mod.getBlogs(q, cb))
        );
    },
    genItems: function(q) {
        var rootPath = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front';
        me.getItem(q, function(r ) {
            require('fs').writeFile(rootPath + '/js/gen/blog/' + q.blog_uid + '.json', JSON.stringify(r.data, null,4), function(e, r){
            });
        });
        require('child_process').exec('cd ' + rootPath + '; bin/ver');
    },
    getItem: function(q, cb) {
        var sql ="SELECT * FROM blog_item WHERE blog_uid=$1 ORDER BY seq";
        UT.db.queryAndCb(sql, [allBlogs[q.handle]], cb);
    },
    saveItem: function(q, cb) {
        q.blog_uid = allBlogs[q.handle];
        var upd1 = upd.gen(q, 'blog_item');
        UT.db.queryAndCb( upd1.sql, upd1.data, cb);
        me.genItems(q);
    },
    reorderItems: function(q, cb) {
        var sql = q.uids.map( function(uid, i) {
            return "UPDATE blog_item SET seq=" + (i+1) + " WHERE uid='" + uid + "'";
        }).join(';');
        UT.db.queryAndCb(sql, [], cb);
    },
    delItem: function(q, cb) {
        var sql = "DELETE FROM blog_item WHERE uid=$1";
        var data = [q.uid];
        UT.db.queryAndCb( sql, data, function(delItemResp) {
            me.getItem(q, cb);
            me.genItems(q);
        });
    },
    getBlogs: function(q, cb ) {
        var sql ="SELECT * FROM blog WHERE domain_uid='" + q.domain_uid + "' ORDER BY seq";
        UT.db.queryAndCb(sql, '', cb);
    },
    saveCat:   function(q, cb) {
        var isCatUpdate = typeof q.cat_uid != 'undefined';

        db.query("SELECT * FROM blog_cat WHERE uid=$1 AND owner_uid=$2" , [q.cat_uid, q.owner_uid], function(checkResp) {
            isCatUpdate = isCatUpdate && checkResp.length > 0;
            var sql = isCatUpdate
                ? "UPDATE blog_cat SET cat_name=$1, sub_cat_name=$2, sub_sub_cat_name=$3 WHERE uid=$4"
                : "INSERT INTO blog_cat (cat_name, sub_cat_name, sub_sub_cat_name, owner_uid) VALUES ($1, $2, $3, $4) RETURNING uid";

            console.log(q, checkResp, ' save cat save resp', isCatUpdate, sql);
            var refUid = isCatUpdate ? q.cat_uid : q.owner_uid;

            var data = [q.cat_name, q.sub_cat_name, q.sub_sub_cat_name, refUid];
            db.query(sql, data, function(blogResp) {
                cb(resp(blogResp, true));
            }, function(err) {
                cb(resp([err, sql, q.cms, q.owner_uid], false));
            });

        }, function(err) {
            console.log( err );
        }
        )
        .catch(e => UT.log.err( e ));
    },
    save:   function(q, cb) {
        var isCatUpdate = typeof q.cat_uid != 'undefined';
        var isUpdate    = typeof q.uid != 'undefined';

        db.query("SELECT * FROM blog_cat WHERE uid=$1 AND owner_uid=$2" , [q.cat_uid, q.owner_uid])
        .then( checkResp => {
            isCatUpdate = isCatUpdate && checkResp > 0;
            var sql = isCatUpdate
                ? "UPDATE blog_cat SET cat_name=$1, sub_cat_name=$2, sub_sub_cat_name=$3 WHERE uid=$4"
                : "INSERT INTO blog_cat (cat_name, sub_cat_name, sub_sub_cat_name, owner_uid) VALUES ($1, $2, $3, $4) RETURNING uid";

            var refUid = isCatUpdate ? q.cat_uid : q.owner_uid;

            Promise.resolve([sql, [q.cat_name, q.sub_cat_name, q.sub_sub_cat_name, refUid]])
        })
        .then( data     => db.query(data[0], data[1]))
        .then( blogResp => cb(resp(blogResp, true)))
        .catch(e => cb(resp(e, false)));
    }
};
