var db = require('./db.js')('cms');
var fs = require('fs')
var UT = require('./util.js');
var log=UT.log;
var upd=require('./updates.js');

var probe = require('probe-image-size');

var rootPath = '/data/files/';
var path   = rootPath + global.srvConfig.inst + '/';
var __path = rootPath + global.srvConfig.inst + '/';

function resp(data, success, msg) {
    success || log.err('FILES ERROR', data);
    typeof msg == 'undefined' && (msg = '');
    return {"data": data, "success": success, "msg":msg};
}

function cbError(e, msg) {
    msg = typeof msg == 'undefined' ? '' : msg;
    var code = typeof e.code == 'undefined' ? '' : e.code;
    UT.log.err( JSON.stringify(e, null, 4) );
    return resp(msg, false, code);
}

function createPath( path ) {
    try  { 
        fs.statSync(path); 
        UT.log.b('FOUND FILE: ' + path); 
        return true; 
    } catch (e) { 
        createPath(path.substring(0, path.lastIndexOf('/')));
        UT.log.b('CREATING FILE: ' + path); 
        fs.mkdirSync(path, 0755);
        return true;
    }
}

function rmdir(dir) {
    var list = fs.readdirSync(dir);
    for(var i = 0; i < list.length; i++) {
        var filename = require('path').join(dir, list[i]);
        var stat = fs.statSync(filename);
        
        if (filename != "." || filename != "..")
            stat.isDirectory()  ? rmdir(filename) 
                                : fs.unlinkSync(filename);
    }
    fs.rmdirSync(dir);
};

function fileStat(path) {
    try {
        path = __path + path;
        var stat = {};
        var fStat    = fs.statSync(path);
        stat.created = fStat.atime.toString().substring(4, 24);

        if (fStat.isDirectory()) {
            var  files = fs.readdirSync(path);
            stat.count = files.length;
        } else {
            var size;
            if (fStat.size < 1000000) size = parseInt(fStat.size/10) / 100 + ' K';
            if (fStat.size > 1000000) size = parseInt(fStat.size/10000) / 100  + ' M';
            stat.size = size;
        }
    } catch (e) {
        UT.log.warn(e.stack, __filename);
    }
    return stat;
}

function getSharedFiles( dbFiles ) {
    var dirs = {};

    dbFiles.map(function(f) {
        if (typeof dirs[f.owner_name] == 'undefined' ) {
            var path = f.owner_uid + '/' + f.dir;
            dirs[f.owner_name] = fileStat( path );
            dirs[f.owner_name].files = [];
        }
    });

    dbFiles.map(function(f) {
        var path = f.owner_uid + '/' + f.dir;
        file = fileStat( path  + '/' + f.name );
        file.name = f.name;
        file.uid  = f.uid;
        file.path = path;
        dirs[f.owner_name].files.push(file);
    });

    for (var d in dirs) dirs[d].count = dirs[d].files.length;

    return dirs;
}

function getMyFiles( dbFiles, owner_uid) {
    var dirs = {};

    dbFiles.map(function(f) {
        if (typeof dirs[f.dir] == 'undefined' ) {
            var path = owner_uid + '/' + f.dir;
            dirs[f.dir] = fileStat( path );
            dirs[f.dir].uid = f.dir_uid;
            dirs[f.dir].files = [];
        }
    });

    dbFiles.map(function(f) {
        if (f.name) {
            var path = owner_uid + '/' + f.dir;
            file = fileStat( path  + '/' + f.name );
            file.name = f.name;
            file.uid  = f.uid;
            file.path = path;
            file.access = f.file_access;
            dirs[f.dir].files.push(file);
        }
    });

    return dirs;
}

function getSharedDirs(owner_uid, cb) {
    var sql = "SELECT u.first || ' ' || u.last as owner_name, f.name AS name, d.name dir, f.uid uid, f.owner_uid, f.dir_uid dir_uid, fa.user_uid " +
              "FROM file_access fa JOIN files f ON (f.uid=fa.file_uid) " + 
              "JOIN dirs d ON (d.uid=f.dir_uid) " + 
              "JOIN users u ON (u.uid=f.owner_uid) " +
              "WHERE fa.user_uid='" + owner_uid + "'";

    var sharedFiles = [];
    db.q(sql, function(dbFiles ) {
        sharedFiles = getSharedFiles(dbFiles, owner_uid, false);
        cb ( resp(sharedFiles, true) );
    });
}

function purgeMissingFilesDb(cb, deletedDirsCount) {
    var delResp = " Deleted " + deletedDirsCount + " user directories ";
    var sql = "SELECT uid, owner_uid as user_dir, owner_uid || '/' || dir || '/' || name AS path  FROM files"
    db.q(sql, function(ret) { 

        var missing = ret.filter(function(p) {
            if (!fs.existsSync(__path + p.path))
                return p.uid;
        })
        .map(function(u) {return u.uid})
        .join("','");

        sql = "DELETE FROM files WHERE uid IN ('" + missing + "')";

        if (missing) 
            db.q(sql,   function( ) { cb(resp("'" + missing + "' Stale files have been deleted. " + delResp, true)); }, 
                        function(e) { cb(resp(e, false)); UT.log.err(e, sql); });
        else 
            cb(resp("No Stale files found." + delResp, true));

    }, function(e) {
        UT.log.err(e, __filename, 'all files ERROR', sql);
        cb ( resp(e, false) );
    });
}

var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index) {
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

function purgeStaleUserDirs(owner_uid, cb) {
    var WHERE = typeof owner_uid == 'undefined' || !owner_uid ? '' : " WHERE uid='" + owner_uid + "' ";
    var sql = "SELECT json_agg(uid) uids  FROM users __WHERE__"; 
    sql = sql.replace('__WHERE__', WHERE);
    log.dbg ( sql );

    var allDirs  = [];
    fs.readdirSync(__path).forEach(function(file,index) {
        allDirs.push( file );
    });

    db.q( sql, function(userList) {
        var dirsToDelete = allDirs.filter(function(dir) {
            return userList[0].uids.indexOf(dir) == -1;
        });

        dirsToDelete.map(function(dir) {
            log.info('DELETING directory', dir );
            deleteFolderRecursive(__path +  dir);
        });
        log.err( dirsToDelete , ' irs ' );
        cb( resp(dirsToDelete, true));
    }, function(e) {
        log.err( e.stack, e.sql);
        cb( resp(e, false) );
    });
}

function purgeStale(cb) {
    purgeStaleUserDirs(null, function(deletedDirs) {
        console.log( deletedDirs, ' delete dirs' , __filename);
        purgeMissingFilesDb(cb, deletedDirs.data.length);
    });
};

function alignFiles(cb) {
    getDirs(function(data) {
        var sql = '';
        var files = [];
        var dirs  = {};

        var dirSql = 'DELETE FROM dirs; DELETE FROM files;\n';
        for (var f in data.data) {
            dirSql += db.qFormat("INSERT INTO dirs (uid, name) VALUES ({0}, '{1}');\n", [db.dbUuid, f]);
        }
        dirSql += "SELECT * FROM DIRS";
        db.q(dirSql, function(dbDirs) {
            dbDirs.map(function(oneDir) { dirs[oneDir.name] = oneDir.uid; });

            for (var f in data.data) {
                data.data[f].files.map(function(file) { 
                    sql += db.qFormat("INSERT INTO files (uid, name, dir_uid, created) VALUES ({0}, '{1}',    '{2}', '{3}');\n", 
                                                                                [db.dbUuid, file.name, dirs[f], file.created]);
                });
            }
            db.q(sql, 
                function(ret) { log.info( 'RET', JSON.stringify(ret)); }, 
                function(err) { log.err( err); }
            );

        }, function(e) { log.err( e.stack ) } );

        getDirs(cb);
    })
}

function updWidth(q, cb) {
    var sql = "SELECT * FROM files";
    db.q(sql, function( files ) {
        files.map( function(f) {
            var fullPath = __path + q.owner_uid + '/' + f.grp + '/' + f.name;
            var imgSize = JSON.stringify(getImgSize( fullPath ));
            sql = "UPDATE files SET img_size='" + imgSize + "' WHERE uid='" + f.uid + "'";
            db.q( sql );
        });
    });
    cb(resp(true, true));
}

function getImgSize( path ) {
    try {
        var imgSize = probe.sync( fs.readFileSync(path) );
        return {w: imgSize.width, h: imgSize.height};
    } catch (e) {
        return null;
    }
}

function getDirs(q, cb) {
    db.query("SELECT * FROM files where domain_uid=$1", [q.domain_uid])
    .then( function( files ) {
        var ret = {};
        files.map( function(f) {
            typeof ret[f.grp] == 'undefined' && (ret[f.grp] = []);
            f.path = f.domain_uid + '/' + f.grp + '/' + f.name;
            ret[f.grp].push(
                {   "name": f.name, 
                    "uid":  f.uid, 
                    "domain_uid" :  f.domain_uid, 
                    "owner_uid":    f.owner_uid, 
                    "img_size":     f.img_size, 
                    "path":         f.domain_uid + '/' + f.grp + '/' + f.name
                }
            );
        });

        cb(resp({"all": files, "grp": ret, owner_uid: q.owner_uid}, true));
    })
    .catch( cbError );
}

function dbUploadFile(q, filePath, cb) {
    var sql = "INSERT INTO files (name, owner_uid, grp, domain_uid, created) " +
              "VALUES ($1, $2, $3, $4, now()) RETURNING uid";

    db.query(sql, [q.fileName, q.owner_uid, q.dirName, q.domain_uid])
    .then( r   => Promise.resolve([JSON.stringify(getImgSize( filePath )), r[0].uid]))
    .then(data => db.query( "UPDATE files SET img_size=$1 WHERE uid=$2 RETURNING img_size", data))
    .then( ()  => getDirs(q, cb) )
    .catch( e => {UT.log.err( [e, ' catch '] ); cb(resp(e, false))});
} 

function writeUploadFile(q, filePath, cb) {
    new Promise( 
        (y,n) => fs.stat( filePath, err => err ? y() : dbUploadFile(q, filePath, cb))
    )
    .then( data => new Promise( (y,n) => {
        var encoding = q.file.indexOf('base64') == -1 ? 'utf8' : 'base64';
        q.ext == 'DAT' && (encoding = 'binary');

        var data = q.file.replace(/^data:.*;base64,/,"")

        fs.writeFile(filePath, data, encoding, err =>
            err === null    ? dbUploadFile(q, filePath, cb)
                            : n([err, 'File Write Error'])
        );
    }))
    .catch( e => {UT.log.err( [e, ' catch '] ); cb(resp(e, false))});
}

module.exports = {
    uploadFile: function(q, cb) {
        var dirPath = __path + q.domain_uid 
            + (typeof q.dirName == 'undefined' ? '' : '/' + q.dirName);

        createPath( dirPath );
        writeUploadFile(q, dirPath + '/' + q.fileName, cb);
    },
    delFile: function(q, cb) {
        db.query("DELETE FROM files WHERE uid=$1 RETURNING *", [q.uid])
        .then( r => new Promise( (y,n) => r.length ? y(r[0]) : n('File not found in DB ' + q.uid)))
        .then( r => {
            try { 
                fs.unlink(`${__path}/${q.domain_uid}/${r.grp}/${r.name}`, () => getDirs(q, cb) )
            } catch (e) { 
                cb( cbError({}, e.toString()) );
            }
        })
        .catch( e => cb( cbError({}, e.toString()) ) );
    },
    updWidth:   updWidth,
    getDirs:    getDirs,
    getSharedDirs:    getSharedDirs,
    alignFiles: alignFiles,
    delDir: function(dirName, owner_uid, cb) {
        rmdir(__path + owner_uid + '/' + dirName);
        var sql = db.qFormat("DELETE FROM files WHERE dir='{0}';DELETE FROM dirs WHERE name='{0}'", 
                        [dirName]);
        log.dbg(sql);
        db.q(sql, function(r) { getDirs(owner_uid, cb); }, cbError);
    },
    delPath: function(q, cb) {
        console.log( q, ' del path q');
        typeof cb == 'function' && cb( resp('good'), true, 'good');
    },
    listAccess : function(owner_uid, cb){
        var sql = db.qFormat("SELECT * FROM files WHERE owner_uid='{0}'", [owner_uid]);

        log.dbg(sql);
        db.q(sql,   function(r) { 
            log.info(r, 'returned from list Access');
            cb(resp(r, true));
        }, 
        function(e) { 
            cb(cbError(e) ); 
        });
    },
    mailFile: function(file_uid, user_email, owner_uid, cb){
        var sql = db.qFormat("SELECT * FROM files WHERE uid='{0}' AND owner_uid='{1}'",
            [file_uid, owner_uid]);

        log.dbg(sql);
        db.q(sql,   function(r) { 
            log.dbg( r, r.length);
            if  (r.length>0) {
                r = r[0];
                r.mail_to = user_email;
                var fPath = __path + owner_uid + '/' + r.dir + '/' + r.name;
                console.log( ' path ' , fPath );
                var file = fs.statSync(fPath);
                var attach = {
                    filename: r.name,
                    path: fPath
                };
                require('./infra')(creds).sendMail(user_email, null, 'Attached File', '...please find attached', attach);
                cb(resp(r, true));
            } else{
                log.warn(' NOT found file ' , r );
                cb(resp('this file does not belong to the user', false));
            }

        }, 
        function(e) { 
            cb(cbError(e) ); 
        });
    },
    grantAccess : function(file_uid, user_uid, owner_uid, cb){
        var sql = db.qFormat("SELECT count(*) count FROM files WHERE uid='{0}' AND owner_uid='{1}'",
            [file_uid, owner_uid]);

        db.q(sql,   function(r) { 
            r = r[0];
            if  (r.count>0) {
                sql = db.qFormat("INSERT INTO file_access (file_uid, user_uid, owner_uid, flag) VALUES ('{0}', '{1}','{2}', 0)", 
                        [file_uid, user_uid, owner_uid]);
                db.q(sql,   function(r) { 
                    cb(resp('Access has been granted', true));
                    global.srvConfig.status[user_uid] |= 0x1;
                }, 
                function(e) { 
                    log.err( sql, e );
                    cb(cbError(e) ); 
                });
            } else{
                log.warn(' NOT found file ' , r );
                cb(resp('this file does not belong to the user', false));
            }

        }, 
        function(e) { 
            cb(cbError(e) ); 
        });
    },
    revokeAccess: function(file_uid, user_uid, owner_uid, cb){
        var sql = db.qFormat("DELETE FROM file_access fa USING files f " +
                             "WHERE f.owner_uid='{0}' AND fa.file_uid='{1}' AND fa.user_uid='{2}'",
                             [owner_uid, file_uid, user_uid]);

        db.q(sql,   function(r) { 
            global.srvConfig.status[user_uid] |= 0x1;
            cb(resp(r, true));
        }, 
        function(e) { 
            log.err(e.stack, sql);
            cb(cbError(e) ); 
        });
    },
    purgeStale: purgeStale,
    purgeStaleUserDirs: purgeStaleUserDirs
}
