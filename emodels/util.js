// POLYFILL until new version
if (false)
if (!Array.from) {
    Array.from = function (object) {
        'use strict';
        return [].slice.call(object);
    };
}

var path = __dirname.split('/');
var models = path.pop();
var dirName = path.pop();

var c = {
    black           :'\x1b[30m',
    blue            :'\x1b[34m',
    green           :'\x1b[32m',
    cyan            :'\x1b[36m',
    red             :'\x1b[31m',
    purple          :'\x1b[35m',
    brown           :'\x1b[33m',
    gray            :'\x1b[37m',
    end             :'\x1b[0m'
};

function resp(data, success) {
    success || console.log('DB ERROR', data);
    data = success ? data :  [data.code, data.detail];
    return {"data": data, "success": success, "msg": ''};
}

function logHeart(arg) {
    try {
        require('fs').appendFile('./log/heartbeat.log', c.gray + arg + c.end + '\n');
    } catch (e) {
        console.log(c.red +  e.stack + c.end);
    }
}
function logArgs(color, args) {
    for (var i in args) {
        console.log(color, JSON.stringify(args[i], null, 4), '\x1b[0m');
    }
}

function doLog(arg, color) {
    try {
        console.log(color, JSON.stringify(arg, null, 4), '\x1b[0m');
    } catch (e) {
        console.log(c.red +  e.stack + c.end);
    }
}

function pad(i) {
    return i<10 ? '0' + i : i;
}
function getZ(dt) {
    var z = {};
    z.yr = dt.getFullYear();
    z.mo = pad(dt.getMonth() + 1);
    z.day = pad(dt.getDate());
    z.hr = pad(dt.getHours());
    z.mi = pad(dt.getMinutes());
    z.sec = pad(dt.getSeconds());
    return z;
}

var DT = {
    later:function(m, h, d, dt) {
        var dt = typeof dt == 'undefined' ? new Date() : new Date(dt);
        if (typeof m != 'undefined' && m) dt.setMinutes(dt.getMinutes() + parseInt(m));
        if (typeof h != 'undefined' && h) dt.setHours(dt.getHours() + parseInt(h));
        if (typeof d != 'undefined' && d) dt.setDate(dt.getDate() + parseInt(d));

        var z = getZ(dt);
        var retDt = z.yr + '-' + z.mo + '-' + z.day + ' ' + z.hr + ':' + z.mi + ':' + z.sec;
        return retDt;
    },
    format:function(dt) {
        var dt = typeof dt == 'undefined' ? new Date() : new Date(dt);

        var z = getZ(dt);
        var retDt = z.yr + '-' + z.mo + '-' + z.day + ' ' + z.hr + ':' + z.mi + ':' + z.sec;
        return retDt;
    }
}

var me = {
    DT: DT,
    isWrAdmin: function(perms) { return perms.substring(0,2) === 'WR'; },
    isAdm :    function(perms) { return perms.substring(0,3) === 'ADM'; },
    isReg :    function(perms) { return perms.substring(0,3) === 'REG'; },
    isClient : function(perms) { return perms.substring(0,3) === 'CLI'; },
    getPerms : function(perms) { return perms.split(':')[0];            },
    parseRequest: function(r, data) {
        var url = require('url');
        var q = data ? data : url.parse(r.url, true).query;
        var p = url.parse(r.url, true).pathname.split('/');
        p[0] == '' && p.shift(); 
        return {p: p, q: q, h: r.headers, s: url.search}
    },

    getColors: function() { return c; },
    colors:  c,
    ucfirst: function(inStr) {
        return inStr.charAt(0).toUpperCase() + inStr.slice(1);
    },
    getDirName: function() { return global.wrRuntime.instance; },
    guid : function() {
        return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + '-' + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    passwd: function() {
        return 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
            + 		Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    resp: function(data, success, msg) {
        success || console.log('INFRA ERROR', data, msg);
        return {"data": success ? data : 'INFRA ERROR', "success": success, "msg": typeof msg == 'undefined' ? '' : msg};
    },
    isUuid: function(uid) {
        return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
    },
    iAm: function(userPerm) {
        return global.wrRuntime.user.perms.trim() == userPerm;
    },
    // Log
    log: {
        lg:     function () { doLog(arguments, '\x1b[32m'); },
        dbg:    function () { doLog(arguments, '\x1b[36m'); },
        warn:   function () { doLog(arguments, '\x1b[33m'); },
        info:   function () { doLog(arguments, '\x1b[34m'); },
        err:    function () { doLog(arguments, '\x1b[31m'); },
        g:      function (arg) { logArgs(c.green,   arguments, c.end); },
        r:      function (arg) { logArgs(c.red,     arguments, c.end); },
        b:      function (arg) { logArgs(c.blue,    arguments, c.end); },
        br:     function (arg) { logArgs(c.brown,   arguments, c.end); },
        p:      function (arg) { logArgs(c.purple,  arguments, c.end); },
        heart:  function (s) { logHeart(s)           }
    }, 
    valid : {
        email: function(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        uid: function(uid) {
            return  uid.toString().match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i);
        }
    },
    db: {
        queryAndCb: function(sql, data, cb) {
            //me.log.plog('... db and query: \n' + sql + ' ' + data.join(','));
            require('./db.js')().query(sql, data)
            .then( userRows => cb(resp(userRows, true, 'OK')) )
            .catch( e => {
                console.log('QCB ERROR', e.stack, sql);
                cb(resp(e, false, 'DB_ERR')); 
            })
        },
        transaction: function(sql, cb) {
            var db = require('./db.js')();
            me.log.rlog(' transaction ');
            db.query('BEGIN;' + sql, [], function(transResult) {
                db.q('COMMIT',  goodCommit =>  cb(transResult), 
                                badCommit  =>  cb(false)
                );
            }, function(err) {
                db.q('ROLLBACK',    goodRollback => cb(false), 
                                    badRollback  => cb(false)
                );
            })
           .catch( e => cb(e) );
        },
        upd: function(q, tab, cb) {
            var p = require('./dbaa.js').gen(q, tab);
            me.log.glog('UPD');
            me.log.blog(p.sql);

            me.db.queryAndCb(p.sql, p.data, cb);
        },
        getDomains: function(cb) {
            var sql = "SELECT name, uid FROM domains";
            require('./db.js')().q(sql, function(domains ) { 
                cb(domains);
            }, e => {
                console.log('QCB ERROR', e.stack, sql);
                cb(resp(e, false, 'DB_ERR')); 
           });
        }
    },
    callAction: function(mod, action, res, q, auth) {
        if (typeof auth == 'undefined' || auth.indexOf(res.sessionUser.creds.split(':')[0]) > -1) {
            q.clinic_uid = res.sessionUser.clinic_uid; 
            q.user_uid   = res.sessionUser.uid;
            mod[action](q, function(r) { res.end(JSON.stringify(r)); });
        } else {
            res.end('{"success": false, "msg": "Insuficient Permissions", "c_uid":"' 
                + res.sessionUser.clinic_uid + '"}');
        }
    },
    sql_types: {
       text: 'varchar(64)',
       checkbox: 'integer',
       radio: 'integer',
       select : 'integer',
       number: 'integer',
       money: 'numeric(8,2)',
       quantity: 'numeric(12,4)',
       qty: 'numeric(12,4)'
    }
}
module.exports = me;
