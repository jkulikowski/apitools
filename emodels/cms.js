var modelPath   = global.srvConfig.modelDir;

var db = require(modelPath + 'db.js')('cms');
var UT  = require(modelPath + 'util.js');
var log = UT.log;

function resp(data, success) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}

var fs = require('fs');
function checkDir(path) {
    return new Promise( (y,n) => {
        fs.stat(path, (e, r) =>
            e === null ? y() : fs.mkdir(path, e => e ? n('Path not created ' + path) : y())
        )
    });
}

var mod = module.exports = {
    get: (q, cb) => { 
        var sql ="SELECT * FROM cms WHERE owner_uid='" + q.owner_uid + "'";
        db.q( sql, cmsResp => cb(resp(cmsResp, true)) );
    },
    save: (q, cb) => {
        var path = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front/js/gen/';

        db.query("SELECT host,uid FROM cms WHERE host=$1", [q.host])
        .then( (foundHost) => Promise.resolve( 
            foundHost.length > 0    ? "UPDATE cms SET cms=$1 WHERE host=$2 RETURNING uid"
                                    : "INSERT INTO cms (cms, host) VALUES ($1, $2) RETURNING uid"
        ))
        .then( s => db.query(s, [q.cms, q.host]) )
        .then( cmsResp => {
            q.cms.uid = cmsResp[0].uid;
            return Promise.resolve(path)
        })
        .then( checkDir ) 
        .then( e => new Promise( (y,n) => 
            fs.writeFile( path + '/cms.json', JSON.stringify(q.cms, null, 4), 
                e => e ? n(e) : cb(resp(q.cms.uid, true)) )
        ))
        .catch( e => { cb(resp(e, false)); UT.log.err( e )} );
    },
    getRoutes: (q, res) => {
        var subDom = res.host.split('.')[0];
        var rtPath = '../front/js/gen/' + subDom + '/routes.js';
        var rt = fs.readFileSync(rtPath + '');
        console.log( rt );
        res.end(rt);
    }
}
