var db   = require('../emodels/db.js')('frame');
var nedb = db.getNe();
var UT   = require('../emodels/util.js');
//var pgauth  = require('../models/admin.js'), url     = require('url');

/*
function emailLoginDb(email, passwd, cb) {
    var sql = "SELECT * FROM users WHERE email=$1 AND passwd=$2";
    db.query(sql, [email, passwd], cb, cb);
}

function verifiedEmailLoginDb(email, passwd, cb) {
    var sql = "SELECT * FROM users WHERE email=$1";
    db.query(sql, [email], cb, cb);
}

function userLoginDb(login, passwd, cb) {
    var sql = "SELECT * FROM users WHERE login=$1 AND passwd=$2";
    db.query( sql, [login, passwd], cb, UT.log.err);
}
*/
var respEnd = (resp, action, success, msg, data, user_uid, puid, name , login) => {
    typeof msg == 'undefined' && (msg='');
    resp.end(JSON.stringify({
        "action":   action, 
        "success":  success, 
        "msg" :     msg, 
        "data" :    data, 
        "user_uid": user_uid, 
        "name" :    name,
        "login":    login,
        "puid":     puid}));
};

function expire() {
    var sessTimeout = 600; //seconds (10 minutes)
    return new Date(new Date().getTime()+(sessTimeout*1000)).toUTCString();
}

function parseCookies (request) {
    var list = {}, rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}

function setCookie(req, resp, loginRes) {
    var sess = {    ip:         req.headers['x-real-ip'], 
                    domain:     req.headers['host'], 
                    cookie:     UT.guid().replace(/-/g,''),
                    creds:      loginRes.perms.trim() + ':frame',
                    login:      loginRes.login,
                    uid:        loginRes.uid,
                    mob:        loginRes.mob,
                    expires:    expire() 
                };

    var srch = {login: loginRes.login, mob:loginRes.mob, ip: sess.ip, domain: req.headers['host']}; 
    nedb.remove(srch, {multi: true}, () => nedb.insert(sess, () => null));

    resp == null || resp.writeHead( 200,
        {'Set-Cookie': 'sessId=' + sess.cookie + ';httpOnly; path=/; expires=' + sess.exipres}
    );

    return sess;
};

var auth = module.exports = {
    apiLogin: (req, cb) => 
        //no-ROUTER ['get', 'apiLogin', '/:userName/:passwd', 'public']
        db.query("SELECT * FROM users WHERE login=$1 AND passwd=$2", [req.params.userName, req.params.passwd])
        .then( loginRes => new Promise( (y,n) => loginRes.length ? y(loginRes[0]) : n('UserName/Password Not Found')) )
        .then( loginRes => cb( 
            {"success" : true, "domain": req.headers['host'], "creds": loginRes.auth + ':' + loginRes.perms, "uid": loginRes.uid},
            setCookie(req, null, loginRes)
        ))
        .catch( e => cb({success: false, creds: e}, {cookie: 'expired'})),
    apiLogout: (req, cb) =>
        nedb.remove({cookie: parseCookies(req).sessId}, {multi: true}, (err, numRemoved) =>
            cb({"logout": true}, {cookie: 'expired', num: numRemoved})
        ),
	checkCookie: function(request, cbOk, cbNoLogin) {
        var cookie = parseCookies(request).sessId;

        if (typeof cookie == 'undefined') { // Work-around for javascript fetch Headers
            var sKey     = request.headers['skey'];
            var swCookie = request.headers['set-cookie'];

            if (typeof swCookie != 'undefined') {  // 01234567
                cookie = swCookie[0].substring(7); // sessId=<cookie>
                auth.updCookie(request, cookie);   // update cookie for SW
            }
        }

        nedb.find({cookie: cookie}, (e1, r) => r.length ? cbOk(r[0]) : cbNoLogin(false));
	}, 
    updCookie: (req, cookie) => {
        cookie = typeof cookie == 'undefined' ? req.headers['sess-cookie'] : cookie;
        nedb.update({cookie: cookie}, {$set: {expires: expire()}},{multi: true, upsert:false});
    },
    destroy: () =>
        nedb.remove({},{multi: true}, (e, r) => 
            respEnd(resp, "destroy", true, r.toString() + ' User Sessions have been cleared.')
        ),
    register:   (req, resp, user) => require('../emodels/infra.js')({}).register(req, resp, user),
    validLogin: (req, resp, user) => respEnd(resp, "login", typeof user != 'undefined' && user && true, 'login check')
    /*
    loginDepr: function(req, resp) {
        //no-ROUTER ['get', 'login', '/:userName/:passwd', 'public']
        var q = url.parse(req.url, true).query;
        UT.valid.email(q.userName) && (q.email = q.userName);

        UT.log.dbg('LOGIN: ' + JSON.stringify(q , null, 0));
        if (q.userName || q.email)
            if (q.email) {
                auth.googleLogin(q, function(googleResp) {
                    if (googleResp.verified_email)
                        verifiedEmailLoginDb(q.email, q.passwd, function(loginRes) {
                            if (loginRes.length) {
                                loginRes     = loginRes[0];
                                loginRes.mob = q.mob;
                                var newSess = setCookie(req, resp, loginRes);
                                respEnd(resp, "login", true, newSess.creds, newSess.cookie, newSess.uid, newSess.puid, loginRes.first + ' ' + loginRes.last,  loginRes.login);
                            } else {
                                respEnd(resp, "login", false, '', '', '', '');
                            }
                        });
                    else 
                        respEnd(resp, "login", false, 'Goggle Email valid but not found in database') ;
                });
            } else {
                userLoginDb(q.userName, q.passwd, function(loginRes) {
                    if (loginRes.length) {
                        loginRes     = loginRes[0];
                        loginRes.mob = q.mob;
                        var newSess = setCookie(req, resp, loginRes);
                        respEnd(resp, "login", true, newSess.creds, newSess.cookie, newSess.uid, newSess.puid, loginRes.first + ' ' + loginRes.last, loginRes.login);
                    } else {
                        respEnd(resp, "login", false, '', '', '', '');
                    }
                });
            }
        else
            respEnd(resp, "login", false, 'User Name required') ;
    },
    logoutDepr: function(req, resp, creds) {
        //no-ROUTER ['get', 'logout', '', 'public']
        var srch = {cookie: creds.cookie};
        nedb.remove(srch, {multi: true}, function(neResp) { 
            respEnd(resp, 'logout', true); 
        });
    },
    googleLogin: function(q, cb) {
        //UT.log.dbg( q,  ' in infra  google' );
        var token = q.token;

        var url="https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=";//ya29.Ci8xA9SFxNOtdGoz7PcNwwt28woJH6Iml-SzWKQvOLT71kE4_MSlKRNznOMoipibFQ
        var refer = '1035165861134-v2q106kf4jgguhl10vlikmpvfq67mlh6.apps.googleusercontent.com';

        require('request')(url + token, function(err, rez, body) {
            body = JSON.parse(body);
            //UT.log.warn('body', body , __filename);

            if (body.issued_to == refer)
                //typeof cb == 'function' && respEnd(body.email, true);
                typeof cb == 'function' && cb(body);
            else
                typeof cb == 'function' && respEnd(null, false);
        });
    }
    */
}
