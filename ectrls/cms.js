var modelPath   = global.srvConfig.modelDir;
var UT          = require(modelPath + 'util.js');
var mod         = require(modelPath + 'cms.js');

module.exports = {
    save: function(q, cb) {
        //ROUTER ['post', 'save']
        mod.save(q, cb);
    },
    get: function(q, cb) {
        //ROUTER ['get', 'get']
        mod.get(q, cb);
    },
    getRoutes: function(q, cb) {
        //ROUTER ['get', 'getRoutes']
        mod.getRoutes(q, cb);
    }
}
