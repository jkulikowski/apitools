var modelPath   = global.srvConfig.modelDir;
var mod         = require(modelPath + 'blog.js');
var UT          = require(modelPath + 'util.js');

module.exports = {
    //ROUTER ['get', 'get', '', 'public']
    get: (q, cb) => mod.get(q, cb),
    //ROUTER ['post', 'saveBlog']
    saveBlog: (q, cb) => mod.saveBlog(q, cb),
    //ROUTER ['post', 'reorderBlogs']
    reorderBlogs: (q, cb) => mod.reorderBlogs(q, cb),
    //ROUTER ['post', 'saveItem']
    saveItem: (res, q) => mod.saveItem(q, cb),
    //ROUTER ['post', 'reorderItems', '']
    reorderItems: (q, cb) => mod.reorderItems(q, cb),
    //ROUTER ['delete', 'delBlog', '/:uid']
    delBlog: (q, cb) => mod.delBlog(q, cb),
    //ROUTER ['delete', 'delItem', '/:uid']
    delItem: (q, cb) => mod.delItem(q, cb),
    //ROUTER ['post', 'saveCat']
    saveCat: (q, cb) => mod.saveCat(q, cb),
    //ROUTER ['get', 'getItem', '/:handle', 'public']
    getItem: (q, cb) => mod.getItem(q, cb),
    //ROUTER ['get', 'getBlogs', '', 'public']
    getBlogs: (q, cb) => mod.getBlogs(q, cb)
}
