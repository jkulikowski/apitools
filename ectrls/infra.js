function OUT(arg) { return JSON.stringify(arg); };
var UT=require('../models/util.js');

module.exports = function(creds) {
    this.mod =  new require('../models/infra.js')(creds);

    return {
        // real-time START 
        saveUser: function(res, q){ 
            // Validation here
            q.puid = res.sessionUser.uid;
            mod.saveUser(q, function(response){ res.end(OUT(response)); })
        },
        saveClient: function(res, p, q){ 
            //require('email-existence').check(q.email, function(emailEr, emailRes) {
                //UT.log.lg( emailRes,' email res', emailEr, ' emailerr');
                //if (emailEr == null && emailRes) {
                    // Validation here
                    q.puid = res.sessionUser.uid;
                    mod.saveClient(q, function(response){ res.end(OUT(response)); })
                //} else {
                    //res.end(OUT({success: false, msg: 'This email address does not exist'}));
                //}
            //});
        },
        pushId : function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.pushId(q, function(response){ res.end(OUT(response)); })
        },
        delUser:     function(res, q) { 
            q.owner_uid = res.sessionUser.uid;
            console.log( q, ' in del ' );
            mod.delUser (q, function(response){ res.end(OUT(response)); })
        },
        delClient:     function(res, q) { 
            var owner_uid = res.sessionUser.uid;
            mod.delClient (owner_uid, q.client_uid, function(response){ res.end(OUT(response)); })
        },
// IMPORTS
        // real-time END
        getUsers: function(res, q) {
            q.puid = res.sessionUser.uid;
            var accessCreds = UT.getPerms(res.sessionUser.creds);

            mod.getUsers(q.puid, function(r) { res.end(OUT(r)); });
        },
        getClients: function(res, q) {
            q.puid = res.sessionUser.uid;
            mod.getClients(q.puid, function(r) { res.end(OUT(r)); });
        },
        getMyCreds: function(res, q) {
            q.uid = res.sessionUser.uid;
            mod.getMyCreds(q.uid, function(r) { res.end(OUT(r)); });
        },
        saveMyCreds: function(res, q) {
            q.puid = res.sessionUser.uid;
            mod.saveMyCreds(q, function(r) { res.end(OUT(r)); });
        },
        google:function(res, q) {
            mod.google(q, function(r) { res.end(OUT(r)); });
        }
    }
}
