var modelPath   = global.srvConfig.modelDir;
var mod         = require(modelPath + 'stock.js');
var UT          = require(modelPath + 'util.js');

module.exports = {
    getDefs: function(q, cb) {
        //ROUTER ['get', 'getDefs', '', 'public']
        mod.getDefs(q, cb);
    },
    saveCat: function(q, cb) {
        //ROUTER ['post', 'saveCat', '']
        mod.saveCat(q, cb);
    },
    saveCatItem: function(q, cb) {
        //ROUTER ['post', 'saveCatItem', '']
        mod.saveCatItem(q, cb);
    },
    delCat: function(q, cb) {
        //ROUTER ['delete', 'delCat', '/:uid']
        mod.delCat(q, cb);
    },
    delCatItem: function(q, cb) {
        //ROUTER ['get', 'delCatItem', '']
        mod.delCatItem(q, cb);
    },
    getStock: function(q, cb) {
        //ROUTER ['post', 'getStock', '', 'public']
        mod.getStock(q, cb);
    },
    saveStock: function(q, cb) {
        //ROUTER ['post', 'saveStock', '']
        mod.saveStock(q, cb);
    },
    saveItem: function(q, cb) {
        //ROUTER ['post', 'saveItem', '']
        mod.saveItem(q, cb);
    },
    delItem: function(q, cb) {
        //ROUTER ['delete', 'delItem', '']
        mod.delItem(q, cb);
    },
    ppHook:function(q, cb) {
        mod.ppHook(q, cb);
    }
}
