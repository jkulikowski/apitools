function OUT(arg) { return JSON.stringify(arg); };
var UT=require('../models/util.js');

function callMod(res, q, creds, method) {
    var perms = creds.creds.split(':');
    q.user_uid   = res.sessionUser.uid;
    q.clinic_uid = res.sessionUser.clinic_uid; 
    console.log( perms ,' perms');

    //if (perms[0] == 'MAR' && perms[2] == 'DEV')
    if (perms[0] == 'SU')
        mod[method](q, function(r) { 
            res.end(OUT(r)); 
        });
    else
        res.end(OUT({data: null, success: false, msg: 'Insuficient permissions to access this controller/action.'}));
}

module.exports = function(creds) {
    this.mod =  new require('../models/dbadmin.js')(creds);

    return {
        createTab:  function(res, p, q) { callMod(res, q, creds, 'createTab'); },
        dropTab:    function(res, p, q) { callMod(res, q, creds, 'dropTab'); },
        getTabDef:  function(res, p, q) { callMod(res, q, creds, 'getTabDef'); },
        addCol:     function(res, p, q) { callMod(res, q, creds, 'addCol');    },
        dropCol:    function(res, p, q) { callMod(res, q, creds, 'dropCol');   }
    }
}
