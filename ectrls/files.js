var modelPath = global.srvConfig.modelDir;

var UT = require(modelPath + 'util.js');

function OUT(arg) { return JSON.stringify(arg); };

var mod = require(modelPath + 'files.js');
var insufficientPerms = '{"success": false, "msg": "Insufficient permissions"}';

module.exports = {
    alignFiles: function(q, cb) {
        //ROUTER ['get', 'alignFiles']
        q.perms == 'SU' ? mod.alignFiles(function(response){ cb(OUT(response)); })
                        : cb(insufficientPerms);
    },
    getDirs: function(q, cb) {
        //ROUTER ['get', 'getDirs', '', 'public']
        mod.getDirs(q, function(response){ cb(OUT(response)); })
    },
    updWidth: function(q, cb) {
        //ROUTER ['get', 'updWidth']
        q.perms == 'SU' ? mod.updWidth(q, function(response){ cb(OUT(response)); })
                        : cb(insufficientPerms);
    },
    getSharedDirs: function(res, q) {
        global.wrRuntime.status[q.owner_uid] &= ~0x2;
        q.owner_uid = res.sessionUser.uid;
        mod.getSharedDirs(q.owner_uid, function(response){ res.end(OUT(response)); })
    },
    listFiles: function(q, cb) {
        //ROUTER ['get', 'listFiles', '']
        mod.getDirs(function(response){ cb(OUT(response)); })
    },
    createDir: function(q, cb) {
        //ROUTER ['get', 'createDir', '']
        mod.createDir(q.dirName, res.sessionUser.uid, function(response){ cb(OUT(response)); })
    },
    renameDir: function(res, q) {
        mod.renameDir(res.sessionUser.uid, q.dir_uid, q.new_name,q.old_name, function(response){ res.end(OUT(response)); })
    },
    delDir: function(q, cb) {
        //ROUTER ['delete', 'delDir', '']
        q.owner_uid = res.sessionUser.uid;
        mod.delDir(q.dirName, q.owner_uid, function(response){ cb(OUT(response)); })
    },
    uploadFile: function(q, cb) {
        //ROUTER ['post', 'uploadFile']
        mod.uploadFile(q, cb);
    },
    delFile: function(q, cb) {
        //ROUTER ['delete', 'delFile', '/:uid']
        //q.owner_uid = res.sessionUser.uid;
        mod.delFile(q, response => cb(OUT(response)) );
    },
    delPath: function(res, q) {
        q.owner_uid = res.sessionUser.uid;
        mod.delPath(q, function(response){ res.end(OUT(response)); 
        })
    },
    listAccess: function(res, q) {
        mod.listAccess(
            res.sessionUser.uid, function(response){ res.end(OUT(response)); 
        })
    },
    grantAccess: function(res, q) {
        typeof global.wrRuntime.status[user_uid] == 'undefined' && (global.wrRuntime.status[q.user_uid] = 0);
        if (UT.valid.uid(q.file_uid) && UT.valid.uid(q.user_uid))
            mod.grantAccess(q.file_uid, q.user_uid, res.sessionUser.uid,
                function(response){ res.end(OUT(response)); 
            })
        else 
            res.end(OUT({success: false, msg: 'Uid format invalid'}));
    },
    revokeAccess: function(res, q) {
        typeof global.wrRuntime.status[user_uid] == 'undefined' && (global.wrRuntime.status[q.user_uid] = 0);
        if (UT.valid.uid(q.file_uid) && UT.valid.uid(q.user_uid))
            mod.revokeAccess(q.file_uid, q.user_uid, res.sessionUser.uid,
                function(response){ res.end(OUT(response)); 
            })
        else 
            res.end(OUT({success: false, msg: 'Uid format invalid'}));
    },
    mailFile: function(res, q) {
        if (UT.valid.uid(q.file_uid) && UT.valid.email(q.user_email))
            mod.mailFile(q.file_uid, q.user_email, res.sessionUser.uid,
                function(response){ res.end(OUT(response)); 
            })
        else 
            res.end(OUT({success: false, msg: 'Uid or email format invalid'}));
    },
    purgeStale: function(q, cb) {
        q.perms == 'SU' ? mod.purgeStale( function(response){ cb(OUT(response)); })
                        : cb(insufficientPerms);
    },
    purgeStaleUserDirs: function(q, cb) {
        q.perms == 'SU' || (q.user_uid && perms == 'ADM')
            ? mod.purgeStaleUserDirs(q.user_uid, function(response){ cb(OUT(response)); })
            : cb(insufficientPerms);
    }
}
