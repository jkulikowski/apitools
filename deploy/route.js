var toolsDir  = global.srvConfig.toolsDir;

var url     = require('url');
var init    = require('./init');
var UT      = require(toolsDir + 'models/util.js');

var toolCtrls = ['auth', 'dbadmin', 'cms', 'stock', 'blog', 'files'];

function getCtrl(name, creds) {
    if (toolCtrls.indexOf(name) > -1) {
        ret = require(toolsDir + 'ctrls/' + name + '.js');
        return typeof ret == 'function' ? ret=ret(creds) : ret;
    } else {
        if (require('fs').existsSync('./ctrls/' + name + '.js')) {
            ret = require('./ctrls/' + name + '.js');
            return typeof ret == 'function' ? ret=ret(creds) : ret;
        } else {
            return false;
        }
    }
}

function domains(rq, cb) {
    switch (rq.p[0]) {
        case 'list' :
            typeof cb == 'function' && cb(JSON.stringify(init.domains));
            break;
        case 'add': 
            UT.db.queryAndCb("INSERT INTO domains (name) VALUES ($1)", [rq.q.name], function(resp) {
                loadDomains( function(doms) {
                    typeof cb == 'function' && cb(doms);
                });
            });
            break;
    }
}

module.exports = {
    go: function(req, res, creds, data) {
        var rq      = init.parseRequest(req, data);
        var ctrl    = rq.p.shift();
        var action  = 'not set';
        var srv     = false;

        ctrl == 'api' && (ctrl = rq.p.shift());

        if (ctrl == 'admin' && creds.perms.trim() == 'REG')
            res.end(JSON.stringify({"success": false, "msg" : "Router: Admin Access Required."}) );
        else if (ctrl == 'domains') 
            domains(rq, function(d) { res.end(d) });
        else try {
            srv     = getCtrl(ctrl, res.sessionUser);
            action  = rq.p.shift();
            UT.log.g( 'CTRL:' + ctrl + '  Action:' + action);

            data && typeof data != 'object' && (rq.q=JSON.parse(data));
            typeof req.headers.authorization == 'undefined' || (rq.q.token = rq.h.authorization);

            if (srv) 
                ctrl == 'auth'  ? srv[action](req, res, creds) 
                                : srv[action](res, rq.q, data, req);
            else                    
                res.end('{"success": false, "msg" : "Router: controller not defined. + ' + ctrl + '"}' );

            require('fs').appendFile('./log/access.log', JSON.stringify(rq.q) + '\n');
        } catch (e) {
            res.end(JSON.stringify({"success": false, "msg" : "Router: route not defined or error occured.", "e": e}) );
            console.log( 'ROUTER catch:' , e.stack , JSON.stringify( e ), srv, action);
        }
    }
}
