var request = require('request');
var fs = require('fs');

var now = new Date().toISOString().substring(0,10);
console.log( now );


var sTpl = '\n\t\t<url>\n\t\t\t<loc>URL</loc>\n\t\t\t<lastmod>NOW</lastmod>\n\t\t\t<changefreq>daily</changefreq>\n\t\t\t<priority>0.8</priority>\n\t\t</url>';
var siteMap = '<?xml version="1.0" encoding="UTF-8"?>\n\t<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">URLS\n\t</urlset>';

var preUrl = 'http://service.prerender.io/https://xyz.enkoda.pro/';
var urls = [
    'land', 'about', 'blog', 'stock', 'contact'
];

var blogItems = Object.keys(JSON.parse(fs.readFileSync('./prerender/blogs.json', 'utf8')));
console.log( blogItems );

//var siteMap = '';
var siteMapUrl = '';
var siteUrls = urls.map(function(a) { return 'https://xyz.encoda.pro/' + a; })
                    .concat( blogItems.map(function(b) { return 'https://xyz.encoda.pro/blog/' + b; }))
                    .map(function(url) { siteMapUrl += sTpl.replace('URL', url).replace('NOW', now); });

siteMap = siteMap.replace('URLS', siteMapUrl );
fs.writeFile( './prerender/sitemap.xml', siteMap);

urls.map(function(url) {
    console.time(url);
    request(preUrl + url, function(error, response, body ) {
        console.log( 'writing: ' + url );
        fs.writeFile('./prerender/' + url + '.html', body);
        console.timeEnd(url);
    });
});
    
blogUrl = preUrl + /blog/;
blogItems.map(function(url) {
    console.time(url);
    console.time(blogUrl + url);
    request(blogUrl + url, function(error, response, body ) {
        console.log( 'writing: ' + url );
        fs.writeFile('./prerender/blog/' + url + '.html', body);
        console.timeEnd(url);
    });
});
