var init = require('./init');
var startWebsockets = false;

var auth        = require(init.toolsDir + 'ctrls/auth.js'),
    UT          = require(init.toolsDir + 'models/util.js'),
    router      = require('./route.js');

require(init.toolsDir + 'models/dbaa.js');

var srv = require('http').createServer(function (request, response) {
    init.setHeaders(response);
    var rq        = init.parseRequest(request);
    response.host = request.headers.host;

  	auth.checkCookie(request, function(user) { // active session
        response.sessionUser = user;

        if (rq.p.indexOf('uploadFile') != -1) {
            new require('formidable').IncomingForm().parse(request, function(err, fields, files) {
                response.writeHead(200, {'content-type': 'application/json'});
                router.go(request, response, user, fields);
            });
        } else {
            if (request.method == 'POST') {
                var body = '';
                request.on('data',function(data) { body += data; });
                request.on('end', function(data) { 
                    router.go(request, response, user, body); 
                });
            } else 
                router.go(request, response, user);

            rq.p.indexOf('validLogin') == -1 && auth.updCookie(request);
        }
	}, function () { // no active session
        if (init.isPublic(rq.p))
            router.go(request, response, init.pubUser, false);
        else
            rq.p.indexOf('login') > -1
                ? auth.login(request, response)
                : response.end('{"success": false, "msg": "Not Authorized"}');
    });
}).listen(global.srvConfig.srvPort );

console.log('API Server running at http://127.0.0.1:/' + global.srvConfig.srvPort );

if (startWebsockets) {
    var m = null;
    var cons= {};
    var wss = require('sockjs').createServer();

    function data(message, connId) { 
        for (var cId in cons) 
            cId == connId || cons[cId].write(message);
    };

    wss.on('connection', function(conn) {
        console.log( ' CONNECTED ON: ' , conn.id);
        conn.on('close', function() { 
            for (var cId in cons)
                if (cId == conn.id) 
                    delete cons[cId];
        });
        conn.on('data', function(msg) { 
            if (msg == '') {
                setTimeout( function() {
                    conn.write(JSON.stringify(["from socket"]));
                }, 20);
            }
            else {
                m = msg;
                data(m, conn.id); } 
            }
        );

        cons[conn.id]  = conn;
        setInterval( function() {
                    conn.write(JSON.stringify(["from socket Interval"]));
        }, 3000);
    });

    wss.installHandlers(srv, {prefix: '/data'});
}

process.on('uncaughtException', function (err) {
    console.log('UNCAUGHT', err.stack);
    require('fs').appendFile('./log/uncaught.log', 
        '------------ BMR API uncaught' + (new Date()).toUTCString() + ' -------------\n' + 
        err.stack + '\n'
    );
}); 
