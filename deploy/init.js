var srvRoot     = '/www/';
var toolsDir    = './tools/'; // always relative
var modDir = toolsDir + 'emodels/';

var UT          = require(modDir + 'util.js');

global.srvConfig         = require(srvRoot + 'clients.js').getByDirPath(__dirname);
global.srvConfig.status  = {};
global.srvConfig.dirName = __dirname;
global.srvConfig.wwwRoot = srvRoot;
global.srvConfig.toolsDir = toolsDir;


var allDomains = {};
function loadDomains(cb) {
    UT.db.getDomains(function( domains ) {
        domains.map(function(d) { allDomains[d.name] = d.uid });
        UT.log.p( allDomains, ' allDomains' );
        typeof cb == 'function' && cb('\n' + JSON.stringify(allDomains));
    });
}

setTimeout( function() {
    require(modDir + 'db.js');
   // require(modDir + 'dbaa.js');
    setTimeout( function() {
        UT.log.dbg(' loading stock and blog', modDir + 'blog.js');
        require(modDir + 'stock.js');
        require(modDir + 'blog.js');
        loadDomains();
    }, 700);
}, 300);

/*
function parseRequest(r) {
    var url = require('url');

    var p = url.parse(r.url, true).pathname.split('/');
    p[0] == '' && p.shift(); 

    var q           = url.parse(r.url, true).query;
    q.host          = r.headers.host;
    q.domain_uid    = allDomains[q.host];

    return {p: p, q: q, h: r.headers, s: url.search};
};
*/

var publicRoutes = [];
function buildRoutes() {
    var routes = require('./gen/rawRoutes.json');
    routes.map( function(r) {
        for (var i in r)
            if (r[i].length)
                publicRoutes = publicRoutes.concat(r[i].filter(function(rr) {
                   return rr.indexOf('public') > -1;
                }).map(function(pb) {
                    return i.replace('tools', 'api') + '/' + pb[1];
                }));
    });
    UT.log.r( 'Public Routes', publicRoutes );
}
buildRoutes();

function parseData(request, response, next) {
    request.domain_uid = allDomains[request.headers.host];
    if (request.method == 'POST') {
        var body = '';
        request.on('data',function(data) { body += data; });
        request.on('end', function(data) { 
            request.body = JSON.parse(body);
            next();
        });
    } else {
        next();
    }
}

function authenticate(req, resp, next) {
    var p   = require('url').parse(req.url, true).pathname.split('/');
    var rt  = [p[1], p[2], p[3]].join('/');
    UT.log.g("IN AUTH CHECK public", rt, publicRoutes.indexOf(rt));
        
    if (publicRoutes.indexOf(rt) > -1)
        next();
    else
        require(toolsDir + 'ctrls/auth.js').checkCookie(req, function(user) {
            console.log( JSON.stringify(user, null, 0), 'user cookie', rt );
            if (typeof user.creds == 'undefined') {
                resp.end('{"success": false, "msg": "Not authrized to access ' + rt + '"}');
            } else {
                req.runtimePerms = user.creds.split(':')[0];
                next();
            }
        });
}

var init = module.exports = {
    srvRoot:        srvRoot,
    toolsDir:       toolsDir,
    //parseRequest:   function(r) { return parseRequest(r) },
    parseData:      function(r, resp, cb) { return parseData(r, resp, cb)    },
    domains:        allDomains,
    authenticate:   authenticate,
    pubUser:        { perms: 'REG' },
    addRuntimePrams:   function(req, xx, next) {  // middleware in router
        req.params.domain_uid = req.domain_uid;
        req.params.perms = typeof req.runtimePerms == 'undefined' ? 'PUBLIC' : req.runtimePerms;
        next(); 
    },
    isPublic:       function(path) { 
        return publicRoutes.indexOf(path.join('/')) > -1; 
    },
    setHeaders: function(response) {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
        response.setHeader("Content-Type", "application/json; charset=utf-8");
    }
}
