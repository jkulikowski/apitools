var tpl =   "\t\tapp.__METHOD('/api/__CTRL/__ACTION__PARAMS',init.addRuntimePrams, function(req, res) {" + 
            //"console.log('moddd', typeof mod, mod);" + 
                "\n\t\t\trequire('.__PATH/ectrls/__CTRL.js').__ACTION(req.params, function(retVal) { res.end( retVal ); });" + 
            "\n\t\t});";

module.exports = {
    sysGen: function() {
        require('child_process').exec('./gen/genRoutes.sh', function(er, resp) {
            resp = '[' + resp.trim().replace(/'/g,      '"')
                                    .replace(/\[,/g,    '[')
                                    .replace(/\{,/g,    '{')
                                    .replace(/\},\]/g,  '}]')
                                    //.substring(1) +
                    ']';

            resp = JSON.parse(resp);
            require('fs').writeFile('./gen/rawRoutes.json', JSON.stringify(resp, null, 4));
            var out = 'var init = require(\'./init.js\');\n' + 
                        'module.exports = {\n\tsetRoutes: function(app) {\n\t\tvar mod;\n';

            resp.map( function(d) {
                var keys  = Object.keys(d)[0];
                var keysA = keys.split('/');
                var key   = keysA.pop();
                var path  = keysA.pop();
                path && (path = '/' + path);

                for (var i in d) 
                    d[i].map( function(dd) {
                        var params = typeof dd[2] == 'undefined' || dd[2] == '' ? '' : dd[2];
                        out += tpl.replace(/__METHOD/,  dd[0])
                                  .replace(/__CTRL/g,   key)
                                  .replace(/__ACTION/g, dd[1])
                                  .replace(/__PATH/g, path)
                                  .replace(/__PARAMS/,  params) + '\n';
                    });

            });
            require('fs').writeFileSync('./appRoutes.js', out + '\t}\n}');
        });
    }
}

console.log(  typeof process.argv[2] == 'undefined' ? 'NOT GENERATING': '...........GENERATING ROUTES..........');
typeof process.argv[2] == 'undefined' || module.exports.sysGen();
