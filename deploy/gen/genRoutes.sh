#!/bin/bash
for i in `ls ./tools/ctrls`; do 
    ctrl=`echo $i | sed "s/\.js//g"`
    echo -n {, \"tools/${ctrl}\": [
    for j in `echo $i | sed "s/\.js//g"`; do 
        cat ./tools/ectrls/${j}.js | grep ROUTER | grep ROUTER | sed "s/^.*\/\/ROUTER/,/g"
    done
    echo -n ]},
done
for i in `ls ./ctrls`; do 
    ctrl=`echo $i | sed "s/\.js//g"`
    echo -n {, \"/${ctrl}\": [
    for j in `echo $i | sed "s/\.js//g"`; do 
        cat ./ctrls/${j}.js | grep ROUTER | grep ROUTER | sed "s/^.*\/\/ROUTER/,/g"
    done
    echo -n ]},
done
echo -n ]
