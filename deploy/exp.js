var express    = require('express');            // call express
var app        = module.exports = express();    // define our app using express

require('./gen/rtgen.js').sysGen();

var init = require('./init.js');
app.use( init.authenticate );
app.use( init.parseData );

app.get('/auth/login', function(req, res) {
    console.log("LOGIN", req.params);
    require('./tools/ectrls/auth').login(req, res);
});

require(init.toolsDir + 'models/dbaa.js');
require('./appRoutes.js').setRoutes(app);

app.listen(global.srvConfig.srvPort);
