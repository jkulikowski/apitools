var modelPath = '../models/';

var UT = require(modelPath + 'util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    this.mod =  new require(modelPath + __filename.split('/').pop())(creds);

    return {
        get: function(res, q) {
            //ROUTER ['get', 'get', '', 'public']
            //q.owner_uid = res.sessionUser.uid;
            mod.get(q, function(response){ res.end(OUT(response)); })
        },
        saveBlog: function(res, q) {
            //ROUTER ['post', 'saveBlog']
            q.owner_uid = res.sessionUser.uid;
            mod.saveBlog(q, function(response){ res.end(OUT(response)); })
        },
        reorderBlogs: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.reorderBlogs(q, function(response){ res.end(OUT(response)); })
        },
        saveItem: function(res, q) {
            //ROUTER ['post', 'saveItem', '/:handle']
            q.owner_uid = res.sessionUser.uid;
            mod.saveItem(q, function(response){ res.end(OUT(response)); })
        },
        reorderItems: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.reorderItems(q, function(response){ res.end(OUT(response)); })
        },
        delBlog: function(res, q) {
            //ROUTER ['get', 'delBlog', '/:uid']
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.delBlog(q, function(response){ res.end(OUT(response)); })
        },
        delItem: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.delItem(q, function(response){ res.end(OUT(response)); })
        },
        saveCat: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.saveCat(q, function(response){ res.end(OUT(response)); })
        },
        getItem: function(res, q) {
            //ROUTER ['get', 'getItem', '/:handle', 'public']
            mod.getItem(q, function(response){ res.end(OUT(response)); })
        }, 
        getBlogs: function(res, q) {
            mod.getBlogs(q, function(response){ res.end(OUT(response)); })
        }
    }
}
