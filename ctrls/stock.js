var modelPath = '../models/';

var UT = require(modelPath + 'util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    this.mod =  new require(modelPath +  __filename.split('/').pop())(creds);

    return {
        getDefs: function(res, q) {
            //ROUTER ['get', 'getDefs', '', 'public']
            mod.getDefs(q, function(response){ res.end(OUT(response)); })
        },
        saveCat: function(res, q) {
            //ROUTER ['post', 'saveCat', '']
            mod.saveCat(q, function(response){ res.end(OUT(response)); })
        },
        saveCatItem: function(res, q) {
            //ROUTER ['post', 'saveCatItem', '']
            mod.saveCatItem(q, function(response){ res.end(OUT(response)); })
        },
        delCat: function(res, q) {
            //ROUTER ['get', 'delCat', '/:uid']
            mod.delCat(q, function(response){ res.end(OUT(response)); })
        },
        delCatItem: function(res, q) {
            //ROUTER ['get', 'delCatItem', '']
            mod.delCatItem(q, function(response){ res.end(OUT(response)); })
        },
        getStock: function(res, q) {
            //ROUTER ['get', 'getStock', '/:uid', 'public']
            q.owner_uid = res.sessionUser.uid;
            mod.getStock(q, function(response){ res.end(OUT(response)); })
        },
        saveStock: function(res, q) {
            //ROUTER ['post', 'saveStock', '']
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.saveStock(q, function(response){ res.end(OUT(response)); })
        },
        saveItem: function(res, q) {
            //ROUTER ['post', 'saveItem', '']
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.saveStock(q, function(response){ res.end(OUT(response)); })
        },
        delItem: function(res, q) {
            //ROUTER ['get', 'delItem', '']
            q.owner_uid = res.sessionUser.uid;
            q.host = res.host;
            mod.delItem(q, function(response){ res.end(OUT(response)); })
        },
        ppHook:function(res, q) {
            mod.ppHook(q, function(response){ res.end(OUT(response)); })
        }
    }
}
