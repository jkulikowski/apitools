var UT = require('../models/util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    //creds.db = 'bmr';
    this.perms = UT.getPerms(creds.creds);
    this.mod =  new require('../models/' +  __filename.split('/').pop())(creds);

    return {
        alignFiles: function(res, q) {
            mod.alignFiles(function(response){ res.end(OUT(response)); })
        },
        getDirs: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
           // global.wrRuntime.status[q.owner_uid] &= ~0x1;
            mod.getDirs(q, function(response){ res.end(OUT(response)); })
        },
        updWidth: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
           // global.wrRuntime.status[q.owner_uid] &= ~0x1;
            mod.updWidth(q, function(response){ res.end(OUT(response)); })
        },
        getSharedDirs: function(res, q) {
            global.wrRuntime.status[q.owner_uid] &= ~0x2;
            q.owner_uid = res.sessionUser.uid;
            mod.getSharedDirs(q.owner_uid, function(response){ res.end(OUT(response)); })
        },
        listFiles: function(res, q) {
            mod.getDirs(function(response){ res.end(OUT(response)); })
        },
        createDir: function(res, q) {
            mod.createDir(q.dirName, res.sessionUser.uid, function(response){ res.end(OUT(response)); })
        },
        renameDir: function(res, q) {
            mod.renameDir(res.sessionUser.uid, q.dir_uid, q.new_name,q.old_name, function(response){ res.end(OUT(response)); })
        },
        delDir: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.delDir(q.dirName, q.owner_uid, function(response){ res.end(OUT(response)); })
        },
        uploadFile: function(res, q, req) {
            //console.log( req,' REQ............');
            q.owner_uid = res.sessionUser.uid;
            mod.uploadFile(q, function(response){ res.end(OUT(response)); })
        },
        delFile: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.delFile(q, function(response){ res.end(OUT(response)); 
            })
        },
        delPath: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.delPath(q, function(response){ res.end(OUT(response)); 
            })
        },
        listAccess: function(res, q) {
            mod.listAccess(
                res.sessionUser.uid, function(response){ res.end(OUT(response)); 
            })
        },
        grantAccess: function(res, q) {
            typeof global.wrRuntime.status[user_uid] == 'undefined' && (global.wrRuntime.status[q.user_uid] = 0);
            if (UT.valid.uid(q.file_uid) && UT.valid.uid(q.user_uid))
                mod.grantAccess(q.file_uid, q.user_uid, res.sessionUser.uid,
                    function(response){ res.end(OUT(response)); 
                })
            else 
                res.end(OUT({success: false, msg: 'Uid format invalid'}));
        },
        revokeAccess: function(res, q) {
            typeof global.wrRuntime.status[user_uid] == 'undefined' && (global.wrRuntime.status[q.user_uid] = 0);
            if (UT.valid.uid(q.file_uid) && UT.valid.uid(q.user_uid))
                mod.revokeAccess(q.file_uid, q.user_uid, res.sessionUser.uid,
                    function(response){ res.end(OUT(response)); 
                })
            else 
                res.end(OUT({success: false, msg: 'Uid format invalid'}));
        },
        mailFile: function(res, q) {
            if (UT.valid.uid(q.file_uid) && UT.valid.email(q.user_email))
                mod.mailFile(q.file_uid, q.user_email, res.sessionUser.uid,
                    function(response){ res.end(OUT(response)); 
                })
            else 
                res.end(OUT({success: false, msg: 'Uid or email format invalid'}));
        },
        purgeStale: function(res, q) {
            if (perms == 'WR') 
                mod.purgeStale( function(response){ res.end(OUT(response)); });
            else 
                res.end(OUT({success: false, msg: 'You are not permitted to perform this operation'}));
        },
        purgeStaleUserDirs: function(res, q) {
            if (perms == 'WR' || (q.user_uid && perms == 'ADM')) 
                mod.purgeStaleUserDirs(q.user_uid,
                    function(response){ res.end(OUT(response)); 
                })
            else 
                res.end(OUT({success: false, msg: 'You are not permitted to perform this operation'}));
        }
    }
}
