var modelPath = '../models/';
var UT = require(modelPath + 'util.js');
function OUT(arg) { return JSON.stringify(arg); };

module.exports = function(creds) {
    //creds.db = 'bmr';
    //this.perms = UT.getPerms(creds.creds);
    this.mod =  new require('../models/' +  __filename.split('/').pop())(creds);

    return {
        save: function(res,  q) {
            q.host = res.host;
            q.owner_uid = res.sessionUser.uid;
            mod.save(q, function(response){ res.end(OUT(response)); })
        },
        get: function(res, q) {
            q.owner_uid = res.sessionUser.uid;
            mod.get(q, function(response){ res.end(OUT(response)); })
        },
        getRoutes: function(res, q) {
            mod.getRoutes(q, res);
        }
    }
}
