var UT  = require('./util.js');
var log = UT.log;
var upd = require('./updates.js');

var db  = require('./db.js')();
var dbErrorMsg = 'Database error occured, please contact administrator at\njarek707@gmail.com';

function resp(data, success, msg) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": msg};
}

function execPrepared(sql, vals, cb) {
    db.query(sql, vals, function(result) { 
        cb(resp(result, true)); 
    },function(err) {
        UT.log.err( sql, vals);
        cb(resp(err, false));
    });
}

var mod = module.exports = function(creds) {
    this.creds = creds;
    this.perms = UT.getPerms(creds.creds);

    var me = null;

    return me = {
        createTab: function(q, cb){
            var sql = "CREATE TABLE " + q.tab_name + " (uid uuid default uuid_in((md5(((random())::text || (now())::text)))::cstring))";

           UT.log.lg( sql );

           db.q(sql, function(colResp){ 
                cb(resp(colResp, true));
           }, function(err) {
                cb(resp([err, sql], false));
           });
        },
        dropTab: function(q, cb){
            var sql = "DROP TABLE " + q.tab_name;

           UT.log.lg( sql );

           db.q(sql, function(colResp){ 
                cb(resp(colResp, true));
           }, function(err) {
                cb(resp([err, sql], false));
           });
        },
        getTabDef: function(q, cb){
            var WHERE = typeof q.col_name == 'undefined' ? '' : ' AND column_name=$2';

            var sql =   "SELECT DISTINCT column_name AS name, data_type AS type, " +
                            "numeric_precision_radix AS radix, numeric_scale AS scale, character_maximum_length AS chars, column_default AS default " + 
                            "FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='public' AND table_name=$1" + WHERE + " ORDER BY column_name";
           UT.log.lg( sql );
           var data = [q.tab_name];
           WHERE && data.push(q.col_name);
           db.query(sql, data, function(colResp){ 
                cb(resp(colResp, true));
           }, function(err) {
                cb(resp([err, sql, data], false));
           });
        },
        addCol: function(q, cb) {
            var DEFAULT = typeof q.default == 'undefined' || !q.default ? '' : ' DEFAULT ' + q.default;
            if ( q.col_type == 'numeric') {
                q.col_type = 'numeric(' + q.radix + ',' + q.scale + ')'; 
            }

            var sql =   "ALTER TABLE " + q.tab_name + " ADD COLUMN " + q.col_name + " " +  q.col_type + ' ' + DEFAULT;
            var data = [q.col_name, q.col_type];
            UT.log.dbg( data, sql, q );

            db.q(sql, function(colResp){ 
                cb(resp(colResp, true));
            }, function(err) {
                cb(resp([err, sql, data], false));
            });
        },
        dropCol: function(q, cb) {
            var DEFAULT = '';
            var sql =   "ALTER TABLE " + q.tab_name + " DROP COLUMN " + q.col_name;

            db.q(sql, function(colResp){ 
                cb(resp(colResp, true));
            }, function(err) {
                cb(resp([err, sql], false));
            });
        }
    }
};
