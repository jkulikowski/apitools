var UT=require('./util.js');

const TXT =0;
const UID =1;
const INT =2;
const UPD =3;
const BOOL=4;
const JSN =5;
const DAT =6;
const TIM =7;
const CHR =8;

var fields = {
    suppliers:{
        active:         BOOL,
        order_count:    INT,
        address:        CHR,
        city:           CHR,
        email:          CHR,
        phone:          CHR,
        product_uid:    UID,
    },
    blog:{
        domain_uid:     UID,
        title:          CHR,
        handle:         CHR,
        content:        JSN,
        img:            CHR,
        hide_img:       BOOL,
        is_vid:         BOOL,
        vid_url:        TXT,
        keywords:       JSN
    },
    blog_item:{
        title:          CHR,
        blog_uid:       UID,
        content:        JSN,
        img:            CHR,
        hide_img:        BOOL,
        keywords:       JSN
    },
    stock: {
        domain_uid:     UID,
        name:           CHR,
        description:    CHR,
        img:            CHR,
        img_size:       JSN,
        cnt:            INT,
        tags:           JSN,
        price:          INT 
    },
    files: {
        domain_uid: UID,
        owner_uid:  UID,
        name:       CHR,
        grp:        CHR,
        img_size:   JSN
    }
};

module.exports = {
    cre: function(key) {
        var sql = 'CREATE TABLE ' + key + ' ( uid uuid not null default uuid_in((md5(((random())::text || (now())::text)))::cstring),';
        var f = fields[key];
        var out = '';
        for (var i in f) {
            switch (f[i]) {
                case CHR:  out += ',\n' + i + ' varchar(128)'; break;
                case TXT:  out += ',\n' + i + ' text'; break;
                case UID:  out += ',\n' + i + ' uuid'; break;
                case INT:  out += ',\n' + i + ' integer'; break;
                case BOOL: out += ',\n' + i + ' boolean'; break;
                case DAT:  out += ',\n' + i + ' date'; break;
                case JSN:  out += ',\n' + i + ' text'; break;
                case TIM:  out += ',\n' + i + ' timestamp'; break;
            }
        }
        sql += out.substring(3) + ');';
        UT.log.dbg( 'CREATE SQL');
        console.log( sql );
    },

    real: function(q, key) {
        var f = fields[key];

        var isUpdate = typeof q.uid != 'undefined' && q.uid != '' && UT.isUuid(q.uid);
        (isUpdate || typeof q.uid != 'undefined') && delete q.uid;

        var ret = {vals: '', flds: '', data: [], warn:[]};
        for (var i in f)
            if ((f[i]!=UID && typeof q[i] != 'undefined') || (f[i]==UID && q[i] !='' )) {
                    var dataVal = [INT, BOOL].indexOf(f[i]) > -1 ? q[i] : "'" + q[i] + "'";
                    if (isUpdate) {
                        ret.flds += ', ' + i + '=' + dataVal + (f[i] == UID ? '::uuid' : '');
                    } else {
                        ret.flds += ', ' + i;
                        ret.vals += ', ' + dataVal + (f[i] == UID ? '::uuid' : '');
                    }
            }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            ret.flds += ', created ';
            ret.vals += ', now()';
        }
        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid='" + q.uid + "'::uuid RETURNING uid, updated";
        } else {
            sql =  "INSERT INTO " + key +  " (" + ret.flds + ") VALUES (" + ret.vals + ") RETURNING *";
        }

        ret.sql = sql;

        console.log('REAL');
        console.log( ret.sql);
    },
    gen: function(q, key) {
        var f = fields[key];
        console.log( 'fiedlds,', f );

        var isUpdate = typeof q.uid != 'undefined' && q.uid != '' && q.uid != null && UT.isUuid(q.uid);
        //isUpdate || (f.created = UPD);

        var ret = {vals: '', flds: '', data: [], warn:[]};
        var idx = 1;
        for (var i in f)
            if (q[i] != 'undefined' && q[i] != null) {
                if (isUpdate) {
                    ret.flds += ', ' + i + '=$' + idx + (f[i] == UID ? '::uuid' : '');
                } else {
                    ret.flds += ', ' + i;
                    ret.vals += ', $' + idx + (f[i] == UID ? '::uuid' : '');
                }

                f[i] == INT  &&  q[i] == '' && (q[i] = 0);
                f[i] == BOOL && (q[i] = q[i] ? 'true' : 'false');
                f[i] == JSN  && (q[i] = JSON.stringify(q[i]));

                ret.data.push(f[i] === INT ? parseInt(q[i]) : q[i]);
                idx++;
            }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            ret.flds += ', created ';
            ret.vals += ', now()';
        }
        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);
        ret.last = '$' + idx;

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid=$" + idx + "::uuid RETURNING uid, updated";
        } else {
            sql =  "INSERT INTO " + key +  " (" + ret.flds + ") VALUES (" + ret.vals + ") RETURNING *";
        }

        //UT.log.dbg(sql);
        ret.sql = sql;

        return ret;
    },

    getReal: function(dbO, key, q) {
        var f = fields[key];
        var ret = [];
        var q = dbO.data;
        console.log( dbO.data, ' dbo data');
        var idx=0;
        for (var i in f) {
            var dataVal = [INT, BOOL].indexOf(f[i]) > -1 ? q[idx] : "'" + q[idx] + "'";
            console.log( i, f[i], dataVal );
            idx++;
        }
        return ret;
    }
}
