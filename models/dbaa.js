var UT=require('./util.js');
UT.log.lg('dbaa used');
var db=require('./db.js')();
var upd = require('./updates.js');

const TXT =0;
const CHR =1;
const UID =2;
const UPD =3;
const BOOL=4;
const JSN =5;
const DAT =6;
const TIM =7;

const INT =8;
const NUM =9;

function buildCols(res) {
    res.map( function(c) {
        typeof cols[c.table_name] == 'undefined' && (cols[c.table_name] = []);
        var cObj = {};
        var tp = CHR;
        switch (c.data_type.substring(0,5)) {
            case 'text'  : tp = TXT;  break;
            case 'chara' : tp = CHR;  break;
            case 'uuid'  : tp = UID;  break;
            case 'date'  :
            case 'times' : tp = DAT;  break;
            case 'time'  : tp = TIM;  break;
            case 'json'  : tp = JSN;  break;
            case 'boole' : tp = BOOL; break;
            case 'integ' : tp = INT;  break;
            case 'numer' : tp = NUM;  break;
        }
        cObj[c.column_name] = tp;//.toString() + ' ' + c.data_type;
        cols[c.table_name].push(cObj);
    });
    //console.log( cols );
}
var cols = null;
function dbCols() {
    if (cols == null) {
        cols = {};
        db.query(
            "SELECT c.table_name, c.column_name, c.data_type FROM INFORMATION_SCHEMA.COLUMNS c " +
            "JOIN INFORMATION_SCHEMA.tables t USING (table_name) " +
            "WHERE t.table_type='BASE TABLE' AND c.table_catalog='cms' AND c.table_schema='public'", [], buildCols, function(err) {
            UT.log.err( err );
        });
    }
};

setTimeout(dbCols, 400);

var fields = {
    'users' : {
        first       : CHR,
        last        : CHR,
        phone       : CHR,
        email       : CHR,
        utype       : INT,
        passwd      : CHR,
        login       : CHR,
        auth        : CHR,      
        perms       : CHR
    }
};

module.exports = {
    cre: function(key) {
        var sql = 'CREATE TABLE ' + key + ' ( uid uuid not null default uuid_in((md5(((random())::text || (now())::text)))::cstring),';
        var f = fields[key];
        var out = '';
        for (var i in f) {
            switch (f[i]) {
                case CHR:  out += ',\n' + i + ' varchar(128)'; break;
                case TXT:  out += ',\n' + i + ' text'; break;
                case UID:  out += ',\n' + i + ' uuid'; break;
                case INT:  out += ',\n' + i + ' integer'; break;
                case BOOL: out += ',\n' + i + ' boolean'; break;
                case DAT:  out += ',\n' + i + ' date'; break;
                case JSN:  out += ',\n' + i + ' text'; break;
                case TIM:  out += ',\n' + i + ' timestamp'; break;
            }
        }
        sql += out.substring(3) + ');';
        UT.log.dbg( 'CREATE SQL');
        console.log( sql );
    },

    real: function(q, tab) {
        var f = fields[tab];

        var isUpdate = typeof q.uid != 'undefined' && UT.isUuid(q.uid);
        isUpdate || (f.created = UPD);

        var ret = {vals: '', flds: '', data: [], warn:[]};
        for (var i in f)
            if (typeof q[i] != 'undefined') {
                    var dataVal = [INT, BOOL].indexOf(f[i]) > -1 ? q[i] : "'" + q[i] + "'";
                    if (isUpdate) {
                        ret.flds += ', ' + i + '=' + dataVal + (f[i] == UID ? '::uuid' : '');
                    } else {
                        ret.flds += ', ' + i;
                        ret.vals += ', ' + dataVal + (f[i] == UID ? '::uuid' : '');
                    }
            }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            ret.flds += ', created ';
            ret.vals += ', now()';
        }
        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            sql =  "UPDATE " + tab + " SET " + ret.flds + " WHERE uid='" + q.uid + "'::uuid RETURNING uid, updated, created";
        } else {
            sql =  "INSERT INTO " + tab +  " (" + ret.flds + ") VALUES (" + ret.vals + ") RETURNING *";
        }

        ret.sql = sql;

        console.log('REAL');
        console.log( ret.sql);
        return ret;
    },
    gen: function(q, key) {
        //module.exports.cre(key);
        module.exports.real(q, key);
        var f = fields[key];

        var isUpdate = typeof q.uid != 'undefined' && UT.isUuid(q.uid);
        isUpdate || (f.created = UPD);

        var ret = {vals: '', flds: '', data: [], warn:[]};
        var idx = 1;
        for (var i in f)
            if (typeof q[i] != 'undefined') {
                    if (isUpdate) {
                        ret.flds += ', ' + i + '=$' + idx + (f[i] == UID ? '::uuid' : '');
                    } else {
                        ret.flds += ', ' + i;
                        ret.vals += ', $' + idx + (f[i] == UID ? '::uuid' : '');
                    }

                    if (f[i] == DAT) {
                        var matched = q[i].match(/^[1-9]{4}-[0-9]{2}-[0-9]{2}$/g);
                    }
                        
                    ret.data.push(f[i] === INT 
                        ? parseInt(q[i])
                        : (f[i] === JSN ? JSON.stringify(q[i]) : q[i]));
                    idx++;
            }

        if (isUpdate) {
            ret.flds += ', updated=now()';
        } else {
            UT.log.lg( ret.flds );
            if (ret.flds.indexOf('created') == -1) {
                ret.flds += ', created ';
                ret.vals += ', now()';
            }
        }

        ret.flds = ret.flds.substring(2);
        ret.vals = ret.vals.substring(2);
        ret.last = '$' + idx;

        var sql = '';
        if (isUpdate) {
            ret.data.push(q.uid);
            sql =  "UPDATE " + key + " SET " + ret.flds + " WHERE uid=$" + idx + "::uuid RETURNING uid, updated, created";
        } else {
            sql =  "INSERT INTO " + key +  " (" + ret.flds + ") VALUES (" + ret.vals + ") RETURNING *";
        }

        ret.sql = sql;

        return ret;
    }
}
