var modelPath = '../models/';

var db = require(modelPath + 'db.js')('cms');
var UT  = require(modelPath + 'util.js');
var log = UT.log;

function resp(data, success) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}

function createDir(dirName, owner_uid, cb) {

    fs.mkdir(path +  owner_uid, 0755, function(e, res ){
        e &&log.info(e.stack, ' continuing' );
    });

    fs.mkdir(path + owner_uid + '/' + dirName, 0755, function(e, res) {
        if (e) {
            log.err(e.stack, ' dir create fs error ' );
            typeof cb == 'function' && cb(resp(cbError(e), false));
        } else {
            var sql = db.qFormat("INSERT INTO dirs (uid, name, owner_uid) VALUES ({0}, '{1}', '{2}')", [db.dbUuid, dirName, owner_uid]);
            log.dbg(sql, ' inside ');
            db.q(sql , function() {
                getDirs(owner_uid, cb);
            }, function(e) {
                log.err( e.stack )
                cb(resp([sql], false));
            });
        }
    });
}
var fs = require('fs');
function checkDir(path, cb) {
    //fs.stat('../front/js/gen/' + q.host.split('.').shift(), function(e, r) {
    //fs.stat('../front/js/gen/', function(e, r) {
    fs.stat(path, function(e, r) {
        if (e === null) {
            cb(true);
        } else {
            fs.mkdir(path, function(e, res ){
                cb(e === null);
            });
        }
    });
}

var mod = module.exports = function(creds) {
    this.creds = creds;

    var me = null;

    return me = {
        get:   function(q, cb) { 
            var sql ="SELECT * FROM cms WHERE owner_uid='" + q.owner_uid + "'";
            db.q(sql, function(cmsResp) {
                cb(resp(cmsResp, true));
            });
        },
        save:   function(q, cb) { 
            var isUpdate  = typeof q.uid != 'undefined';

            db.query("SELECT host,uid FROM cms WHERE uid=$1 AND host=$2", [q.uid, q.host], function(checkResp) {
                var sql = isUpdate && checkResp.length > 0
                    ? "UPDATE cms SET cms=$2 WHERE owner_uid=$1 AND host=$3 RETURNING uid"
                    : "INSERT INTO cms (owner_uid, cms, host) VALUES ($1, $2, $3) RETURNING uid";

                var data = [q.owner_uid, q.cms, q.host];
                db.query(sql, data, function(cmsResp) {
                    q.cms.uid=cmsResp[0].uid;

                    var path = global.srvConfig.wwwRoot + q.host.split('.').shift() + '/front/js/gen/';
                    UT.log.lg('save path', path + 'cms.json',q.host);
                    checkDir(path, function(dirStatus) {
                        if (dirStatus) {
                            require('fs').writeFile(path + '/cms.json', JSON.stringify(q.cms,null,4), function(e, r){
                                e && console.log('e',  e );
                                r && console.log('r',  r );
                            });
                            cb(resp(cmsResp, true));
                        } else {
                            cb(resp(cmsResp, false));
                        } 
                    });

                }, function(err) {
                    cb(resp([err, sql, q.cms, q.owner_uid], false));
                });

            });
        },
        getRoutes: function(q, res) {
            var subDom = res.host.split('.')[0];
            var rtPath = '../front/js/gen/' + subDom + '/routes.js';
            var rt = fs.readFileSync(rtPath + '');
            console.log( rt );
            res.end(rt);
        }
    }
};
