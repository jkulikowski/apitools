var UT  = require('./util.js');
var log = UT.log;
/*
GOOLGE client stuff
cxeV6qUXI6NSHvdKTLB6yRnj
1035165861134-v2q106kf4jgguhl10vlikmpvfq67mlh6.apps.googleusercontent.com
*/

var nm  = require('nodemailer');

var gcm = require('node-gcm');
//var apiKey = 'AIzaSyDRpp6ne8leBimATjx7RtLGFBmhwcrop8k';
var apiKey = 'AIzaSyAvQVg0_ajhS9lGHatoQoPVSHwPnSoNhXs'; // devServer
function doPush(pushId, notif, cb) {

    var sender          = new gcm.Sender(apiKey); //API Server Key
    log.info( 'SENDER ', sender );
    var registrationIds = [];
    var message         = new gcm.Message({});

    message.to = pushId;
    message.priority =  "high";
    message.content_available=true;
    message.addNotification({
        title: notif.title,
        body: notif.body,
        badge : "1",
        sound : "default",
        icon: "ic_small_notification",
        color: "#007D43",
        click_action: "https://dev.jktools.pro",
        url: 'https://dev.jktools.pro'
    });

    typeof pushId=='string'
        ? registrationIds.push(pushId)
        : registrationIds = pushId; // better be an array since it is not a string

    if (true) {
        pushId && sender.send(message, registrationIds, 4, function (err, result) {
            err && console.log(err);
            err || console.log( result );
        });
    } else {
        cb({success: true, msg:'No registrationIds provided - server error'});
        logrule('No registrationIds provided - server error', notifId, true);
    }
};

var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'jarek707@gmail.com',
        pass: 'donkey99'
    }
};

var trans = nm.createTransport(smtpConfig);

function verifyEmail(email, cb, cbErr) {
    return new Promise( function(y, n) {
        require('email-existence').check(email, function(emailEr, emailRes) {
           emailEr == null && emailRes ? y() : n();
        });
    });
}

//if (false)
function sendMail(to, aHref, subject, text, attach) {
    verifyEmail(to)
    .then(function() {
        log.warn('sendMail', arguments );
        var opts ={
                from:   "Admin<jarek@dev.jktools.pro>", // sender address
                to:     to
        };

        typeof attach == 'object' && (opts.attachments = attach);
        if (typeof aHref == 'string') 
            opts.html = '<style>a {font-size:1.8em;}</style><hr />' +
                            '<a href="'  + aHref + '">' +
                            '<img src="https://dev.jktools.pro/image/tool-logo-96.png"></img>' +
                        ' ...click here to log in</a><hr />' + 
                        'Your account has been created successfully. ' +
                        'Click on the link above to activate it.';

        opts.subject = typeof subject == 'undefined' ? "Your Login Credentials" : subject
        opts.text    = typeof text == 'undefined' ? 'The following is login information for dev.jktools.pro' : text;

        trans.sendMail(opts, function(e, r) {
            console.log( ' returned from EMAIL');
            e && console.log( e ) ;
            e || console.log( r );
        });
    })
    .catch(function() {
        log.warn('Email address: ' + email + ' was not found. Message aborted.');
    });
}

function sendInvite(email, login, passwd) {
    verifyEmail(email)
    .then(function() {
        sendMail(email, "https://dev.jktools.pro/setup?userName=" + login + "&passwd=" + passwd + "&signup=true");
        log.info(email, "https://dev.jktools.pro/setup?userName=" + login + "&passwd=" + passwd);
    })
    .catch(function() {
        log.warn('Email address: ' + email + ' was not found.' ,'Added inserted new user with invalid email and added to client\'s list');
    });
}

var db  = require('./db.js')();
var dbErrorMsg = 'Database error occured, please contact administrator at\njarek707@gmail.com';

var infraQ = {
    "allUsers"     :   "SELECT u.uid, u.first, u.last, u.phone, u.email, u.flag::text AS auth_flag, u.reg AS reg_flag, u.pushid, u.created, u.updated, " +
                    "u.perms, u.login, u.passwd, u.puid " +
                    "FROM users u " +
                    "ORDER BY last,first, created",
    "users"     :   "SELECT u.uid, u.first, u.last, u.phone, u.email, u.login, u.perms " +
                    "FROM users u " +
                    "WHERE u.uid IN (SELECT cuid FROM clients WHERE {PUID} UNION SELECT puid FROM clients WHERE {CUID}) " +
                    "ORDER BY last,first, created",
    "del"       :   "DELETE FROM {0} WHERE uid='{1}'",
};

function columnDefs (type, creds) {
    var credsObj = {
        'users': {
            'WR' : [ 
                { name: "Auth",        type: "select",   dbName: "perms",    cls: "small",   vis: true  , srch: true},
                { name: "Login",       type: "text",     dbName: "login",    cls: "",         vis: true},
                { name: "Passwd",      type: "text",     dbName: "passwd",   cls: "",         vis: false},
                { name: "Prénom",      type: "text",     dbName: "first",                     vis: true  },
                { name: "Nom",         type: "text",     dbName: "last",                      vis: true  },
                { name: "Email",       type: "text",     dbName: "email",    cls: "xlarge",   vis: true  },
                { name: "Cellulaire",  type: "text",     dbName: "phone",                     vis: false },
                { name: "Reg",         type: "checkbox", dbName: "reg_flag", cls: "checkbox", vis: false },
                { name: "Created",     type: "text",     dbName: "created",  cls: 'xlarge',   vis: false },
                { name: "Updated",     type: "text",     dbName: "updated",  cls: 'xlarge',   vis: false },
                { name: "Uid",         type: "readonly", dbName: "uid",      cls: "huge",     vis: false },
                { name: "Sent At",     type: "text",     dbName: "sent_at",  cls: "",         vis: false },
                { name: "Puid",        type: "readonly", dbName: "puid",     cls: "huge",     vis: false },
                { name: "Push ID",     type: "text",     dbName: "pushid",   cls: "huge",     vis: false }
            ],
            'ADM' : [ 
                { name: "Login",       type: "text",     dbName: "login",    cls: "",         vis: true},
                { name: "Prénom",      type: "text",     dbName: "first",                     vis: true  },
                { name: "Nom",         type: "text",     dbName: "last",                      vis: true  },
                { name: "Email",       type: "text",     dbName: "email",    cls: "xlarge",   vis: true  },
                { name: "Cellulaire",  type: "text",     dbName: "phone",                     vis: false }
            ],
            'CLI' : [ 
                { name: "Prénom",      type: "text",     dbName: "first",                     vis: true  },
                { name: "Nom",         type: "text",     dbName: "last",                      vis: true  },
                { name: "Email",       type: "text",     dbName: "email",    cls: "xlarge",   vis: true  },
                { name: "Cellulaire",  type: "text",     dbName: "phone",                     vis: true  }
            ]
        }
    }
    return credsObj[type][creds];
};

function resp(data, success) {
    success || log.err('INFRA ERROR', data);
    return {"data": data, "success": success, "msg": ''};
}

/*
function getUsers(puid, cb, client) {
    var sql = infraQ.users;
    sql = sql.replace('{PUID}', (puid == null ? 'false' : " puid='" + puid + "'"));

    log.dbg( 'GET USERS',sql );
    db.q(sql, function(userRows ) { 
            var data = {
                "users":    userRows,
                "selects": { "perms": [ 
                                        { name: "WR",  idx: "WR" },
                                        { name: "ADM",   idx: "ADM" },
                                        { name: "REG",   idx: "REG" },
                                        { name: "CLI",   idx: "CLI" }
                
                ]},
                "columns": columnDefs('users', 'WR')
            };
            cb(resp(data,true)); 
    }, function(e) {
        //log.err( e.stack );
        console.log( e.stack );
        cb(resp([], false)); 
   });
};
*/

function prepIngest(q, perms) {
    q.reg_flag  == null && (q.reg_flag = false);
    q.reg_flag  == '' && (q.reg_flag = 'false');
    q.auth_flag == null && (q.auth_flag = -1);
    (typeof q.auth_flag == 'undefined' || q.auth_flag === '') && (q.auth_flag = 1);
    q.first == '.' && q.last == '' && (q.first = q.email);

    var cols = {};
    switch (perms) {
        case 'WR' : 
            cols = {
                pushid: [q.pushid],
                flag:   [q.auth_flag],
                reg:    ['true'],
            }
        case 'ADM' : 
            typeof q.login == 'undefined' && (q.login = q.email);
            cols = require('util')._extend(cols, {
                first:  [q.first],
                last:   [q.last],
                phone:  [q.phone, 'not empty'],
                email:  [q.email, 'not empty'],
                perms:  [q.perms ? q.perms : 'CLI'],
                login:  [q.login]
            });
            break;
        default: break;
    }
    typeof q.passwd == 'undefined' || (cols.passwd=[q.passwd]);

    return cols;
}

function insertUser(q, cb, perms) {
    var cols = prepIngest(q, perms);
    cols.uid = [db.dbUuid, 'no quote'];

    var sql = 'INSERT INTO users ' + db.qFmt(cols, false) + ' RETURNING *';

    db.q(sql, function (res) {
        cb( res[0] );
    }, function(e) {
        log.err(e.stack);
        cb( {success: false} );
    });
}

function checkUserByLogin(login, cb) {
    var sql = "SELECT uid from users WHERE login=$1";
    db.query(sql, [login], function(loginRes) {
        console.log( loginRes );
        cb(loginRes.length);
    }, function(err) {
        UT.log.err(err, sql);
        cb(false);
    });
}

var mod = module.exports = function(creds) {
    this.creds = creds;
    this.perms = UT.getPerms(creds.creds);

    var me = null;

    return me = {
        connStr: this.connStr,
        end: function() { this.pgClient.end(); },

        pushId: function(q, cb) {
            var pushId = q.id.split('/').pop();
            cb(resp(pushId, true));
            var sql = "UPDATE users SET pushid='" + pushId + "' WHERE uid='" + q.owner_uid +"' RETURNING *";
            db.q(sql, function(pushRes) {
                cb(resp(pushRes, true));
            });
        },
        getUsers:   function(puid, cb, inPerms) { 
            var sql     = infraQ.users;
            var puidReg = puid == null ? 'false' : " puid='" + puid + "' ";
            var cuidReg = puid == null ? 'false' : " cuid='" + puid + "' ";

            if (inPerms == 'WR' ) 
                sql = infraQ.allUsers;
            else
                sql = sql.replace('{PUID}', puidReg).replace('{CUID}', cuidReg);

            log.info( sql );
            db.q(sql, function(userRows ) { 
                    var data = {
                        "users":    userRows,
                        "selects": { "perms": [ 
                                                { name: "WR",  idx: "WR" },
                                                { name: "ADM",   idx: "ADM" },
                                                { name: "REG",   idx: "REG" },
                                                { name: "CLI",   idx: "CLI" }
                        
                        ]},
                        "columns": columnDefs('users', inPerms)
                    };
                    cb(resp(data,true)); 
            }, function(e) {
                //log.err( e.stack );
                log.err( e.stack, sql);
                cb(resp(dbErrorMsg , false)); 
                /* this is the error handler - a separate cb */
           });
        },
        sendMail: function(to, aHref, subject, text, attach) {
            sendMail(to, aHref, subject, text, attach);
        },
        saveUser: function(q, cb) {
            log.err( q );
            var sql = "SELECT uid FROM users where email='" + q.email + "'";
            db.q(sql, function(emailResp) {
                cb(resp(emailResp, true));
                
                var foundUid = emailResp.length ? emailResp[0].uid : false;
                var cols = prepIngest(q, perms);

                var isUpdate = typeof q.uid != 'undefined' && q.uid !== '' && q.uid !== null;

                if (isUpdate) {
                    sql = 'UPDATE users SET ' + db.qFmt(cols, true) + " WHERE uid='" + q.uid + "' RETURNING *";
                    log.dbg( sql, ' updated' );
                } else {
                    cols.uid = [db.dbUuid, 'no quote'];
                    if (foundUid)   sql = "SELECT '" + foundUid + "' AS uid";
                    else            sql = 'INSERT INTO users ' + db.qFmt(cols, false) + ' RETURNING *';
                }

                db.q(sql, function (res) {
                    if (!isUpdate)  {
                        sql = db.qFormat("INSERT INTO clients (puid, cuid, flag) VALUES ('{0}', '{1}' , 0)", [q.puid, res[0].uid]);
                        db.q(sql, function(cliRes) {
                            me.getUsers(q.puid, cb);
                        });
                    } else me.getUsers(q.puid, cb);
                }, function (ex) {
                    log.err(JSON.stringify(ex, null, 4) );
                    cb(resp(JSON.stringify(ex) + sql, false));
                });
            }, function(err) {
                log.err(JSON.stringify(err, null, 4) );
                cb(resp(JSON.stringify(err) + sql, false));
            });
        },
        delClient: function(owner_uid, client_uid, cb) {
            var sql = "DELETE FROM clients WHERE puid='" + owner_uid + "' AND cuid='" + client_uid + "'";
            db.q(sql, function (res) {
                me.getUsers(owner_uid, cb, 'ADM');
            }, function (ex) {
                log.err(ex.stack);
                cb(resp(ex, false));
            });
        },
        delUser: function(q, cb) {
            var sql = db.qFormat("DELETE FROM users where uid='{0}'; DELETE FROM clients WHERE puid='{0}' OR cuid='{0}'", [q.params]);

            db.q(sql, function (res) {
                me.getUsers(q.owner_uid, cb);
            }, function (ex) {
                log.err(ex);
                cb(resp(ex, false));
            });
        },
        getMyCreds: function(myUid, cb) {
            var sql = db.qFormat("SELECT first, last, phone FROM users WHERE uid='{0}'", [myUid]);

            db.q(sql, function (res) {
                cb(resp(res[0], true));
            }, function (ex) {
                log.err(ex);
                cb(resp(ex, false));
            });
        },

        googleDepr: function(q, cb) {
            log.dbg( q,  ' in infra  google' );
            var token = q.token;

            var url="https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=";//ya29.Ci8xA9SFxNOtdGoz7PcNwwt28woJH6Iml-SzWKQvOLT71kE4_MSlKRNznOMoipibFQ
            var refer = '1035165861134-v2q106kf4jgguhl10vlikmpvfq67mlh6.apps.googleusercontent.com';

            require('request')(url + token, function(err, rez, body) {
                body = JSON.parse(body);
                log.info( body );
                if (body.issued_to == refer)
                    typeof cb == 'function' && cb(resp(body.email,true));
                else
                    typeof cb == 'function' && cb(resp(null,false));
            });
            //res.end(OUT(response)); 
        },
        register: function(req, resp, user) {
            var q = url.parse(req.url, true).query;
            var vv = require('email-existence');

            checkUserByLogin(q.login, function(loginExists) {
                UT.log.dbg( loginExists , ' login exists ');
                vv.check(q.email, function(e, r) {
                    if (r) {
                        console.log(' adding user to db');
                        var sm = require('sendmail')();
                        if (false)
                        sm({
                            from:'jarek707@gmail.com',
                            to: q.email,
                            subject: 'Your registration credentials',
                            body:'CREDS come here'
                        }, function(e, r) {
                            //console.log('mail e', e );
                            //console.log('mail r', r );
                            UT.log.err( err , ' register ');
                        });
                        var sql = "INSERT INTO users (login, passwd, domain, email, auth, perms) " +
                                    "VALUES ($1, $2, $3, $4, 'REG', 'REG') RETURNING uid,domain";
                        var data = [q.user, q. passwd, UT.passwd(), q.email];
                        db.query(sql, data, function(rr) {
                            console.log( rr[0] );
                            respEnd(resp, rr, r, '');
                        }, function(err) {
                            respEnd(resp, err, false, '');
                            UT.log.err( err, ' register error ');
                        });
                    } else {
                        console.log(' NOT adding user to db');
                        respEnd(resp, ' register', false, 'Invalid Email');
                    }
                });
            });
        },
    }
};
